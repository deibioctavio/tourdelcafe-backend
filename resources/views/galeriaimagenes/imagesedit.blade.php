@extends('app')
@section('content')
@include('partials.galeriaimagenes.imagesedit')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Galería de Imágenes</h2>
            <h4>Seleccione las imágenes que harán parte de la Galaría ( 18 en Total )</h4>

            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('galeriasetimagenes', [])}}" method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}" />

              <input type="hidden" value="0" name="images_count" id="images_count">

              <div class="form-group right">
                  <label class="col-sm-8">Total Imágenes Seleccionadas</label>
                  <div class="col-sm-4" id="images_count">##</div>
              </div>
              <hr class="mt10 mb20">
              <div class="table-responsive">
                  <table class="table table-hover table-bordered report">
                      <thead>
                          <tr>
                              <th><center>#</center></th>
                              <th><center>Nombre</center></th>
                              <th><center>Imagen</center></th>
                              <th><center>Incluir en la Galería</center></th>
                          </tr>
                      </thead>
                      <tbody>
                          @if (count($gi) < 1)
                              <tr>
                                  <td colspan="4">No existen imágenes registradas</td>
                              </tr>
                          @elseif (count($gi) >= 1)
                              @foreach ($gi as $a)
                                  <tr>
                                      <td>{{ $a->id }}</td>
                                      <td>{{ $a->nombre }}</td>
                                      <td><center>
                                          <img src="{{ env('BASE_PUBLIC_URL_PATH'). 'images/galeria/'.$a->nombre_imagen_guardada}}" alt="{{ $a->nombre_imagen_original }}" width="128px" height="72"/>
                                      </center></td>
                                      <td><?php
                                        $checked = ($a->belongs_to_galery == 0)?"":"checked";
                                      ?>
                                        <input
                                                type="checkbox"
                                                id="imagegalery-add"
                                                name="image_galery[]"
                                                value="{{ $a->id }}"
                                                {{ $checked }}
                                        />
                                      </td>
                                  </tr>
                              @endforeach
                          @endif
                      </tbody>
                  </table>
              </div>
              <div>
                  <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
              </div>
              <hr class="mt10 mb40">
            </form>
        </div>
    </div>
</div>
@endsection
