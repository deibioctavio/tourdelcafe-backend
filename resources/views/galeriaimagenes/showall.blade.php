@extends('app')

@section('content')
@include('partials.galeriaimagenes.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Galería de Imágenes</h2>
            <h4>Listado de imágenes disponibles</h4>
            <div class="form-group right">
                    <a href="{{route('galeriaimagenesadd')}}" class="text-white">
                        Adicionar Imagen
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered report">
                    <thead>
                        <tr>
                            <th><center>#</center></th>
                            <th><center>Nombre</center></th>
                            <th><center>Nombre Imagen</center></th>
                            <th><center>Nombre Thumbnail</center></th>
                            <th><center>Posición</center></th>
                            <th><center>Activo</center></th>
                            <th><center>Imágen</center></th>
                            <th><center>Imágen Thumbnail</center></th>
                            <th colspan="2"><center>Acción</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($gi) < 1)
                            <tr>
                                <td colspan="10">No existen imágenes registradas</td>
                            </tr>
                        @elseif (count($gi) >= 1)
                            @foreach ($gi as $a)
                                <tr>
                                    <td>{{ $a->id }}</td>
                                    <td>{{ $a->nombre }}</td>
                                    <td>{{ $a->nombre_imagen_guardada }}</td>
                                    <td>{{ $a->nombre_thumb_imagen_guardada }}</td>
                                    <td>{{ $a->posicion }}</td>
                                    <td><center>
                                        <?php echo ($a->active==1)?"Si":"No"?>
                                    </center></td>
                                    <td><center>
                                        <img src="{{ env('BASE_PUBLIC_URL_PATH'). 'images/galeria/'.$a->nombre_imagen_guardada}}" alt="{{ $a->nombre_imagen_original }}" width="128px" height="72"/>
                                    </center></td>
                                    <td><center>
                                        <img src="{{ env('BASE_PUBLIC_URL_PATH'). 'images/galeria/'.$a->nombre_thumb_imagen_guardada}}" alt="{{ $a->nombre_thumb_imagen_guardada }}" width="72px" height="72px"/>
                                    </center></td>
                                    <td><center>
                                        <a href="<?php echo url('galeriaimagenesedit',[$a->id])?>">Actualizar</a>
                                    </center></td>
                                    <td><center>
                                      <a id="galeriaimagenes-del" number="<?php echo $a->id?>"  href="<?php echo url('galeriaimagenesdelete',[$a->id])?>">Eliminar</a>
                                    </center></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
