@extends('app')
 
@section('content')
@include('partials.datepicker')
@include('partials.profiles.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Perfil</h2>
            <h4>Diligencie la información del Perfil</h4>
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('profileadd', [])}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                
                <div class="form-group right"> 
                    <label class="col-sm-3">nombre de usuario</label>
                    <div class="col-sm-9">
                        <input type="text" value="<?php echo $user->name?>" name="user_name" class="form-control" readonly>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre</label>
                    <div class="col-sm-9">
                        <input type="text" value="" name="name" id="name" class="form-control">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Apellido</label>
                     <div class="col-sm-9">
                        <input type="description" value="" name="lastname" id="lastname" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Tipo de documento de identificación</label>
                     <div class="col-sm-9">
                        <select name="document_type" id="document_type">
                            <option value="CC">Cédula de Ciudadanía</option>
                            <option value="CE">Cédula de Extrangería</option>
                            <option value="PP">Pasaporte</option>
                            <option value="TI">Tarjeta de Identidad</option>
                        </select>
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Número de documento</label>
                     <div class="col-sm-9">
                        <input type="description" value="" name="document_number" id="document_number" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Fecha de Nacimiento</label>
                     <div class="col-sm-9">
                        <input type="description" value="" name="birthday" id="birthday" class="form-control" readonly>
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Dirección</label>
                     <div class="col-sm-9">
                        <input type="description" value="" name="address" id="address" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Género</label>
                     <div class="col-sm-9">
                        <select name="gender" id="gender">
                            <option value="M">Masculino</option>
                            <option value="F">Femenino</option>
                            <option value="O">Otro/No Aplica</option>
                        </select>
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Teléfono</label>
                     <div class="col-sm-9">
                        <input type="description" value="" name="phone" id="phone" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Celular</label>
                     <div class="col-sm-9">
                        <input type="description" value="" name="cellphone" id="cellphone" class="form-control">
                     </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection