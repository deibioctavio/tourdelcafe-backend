@extends('app')

@section('content')
@include('partials.ubicacion.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Ubicaciones (Marker-Google Map)</h2>
            <h4>Listado de Ubicaciones/Markers Registrados</h4>
            <div class="form-group right">
                    <a href="{{route('ubicacionadd')}}" class="text-white">
                        Adicionar Ubicación
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered report">
                    <thead>
                        <tr>
                            <th><center>#</center></th>
                            <th><center>Latitud</center></th>
                            <th><center>Latitud</center></th>
                            <th><center>Nombre</center></th>
                            <th><center>Descripción</center></th>
                            <th><center>Dirección</center></th>
                            <th><center>Teléfono</center></th>
                            <th><center>Categoría</center></th>
                            <th><center>Ícono</center></th>
                            <th colspan="2"><center>Acción</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($ubicaciones) < 1)
                            <tr>
                                <td colspan="11">No existen Ubicaciones/Markers registrados</td>
                            </tr>
                        @elseif (count($ubicaciones) >= 1)
                            @foreach ($ubicaciones as $a)
                                <tr>
                                    <td>{{ $a->id }}</td>
                                    <td>{{ $a->latitud }}</td>
                                    <td>{{ $a->longitud }}</td>
                                    <td>{{ $a->nombre }}</td>
                                    <td>{{ $a->descripcion }}</td>
                                    <td>{{ $a->direccion }}</td>
                                    <td>{{ $a->telefono }}</td>
                                    <td>{{ $a->categoria }}</td>
                                    <td><center>
                                        <img src="{{env('BASE_PUBLIC_URL_PATH').'images/markers/'.$a->nombre_icono}}" alt="{{$a->nombre_icono}}">
                                    </center></td>
                                    <td><center>
                                    <a href="<?php echo url('ubicacionedit',[$a->id])?>">Actualizar</a>
                                    </center></td>
                                    <td><center>
                                    <a id="ubicacion-del" number="<?php echo $a->id;?>"  href="<?php echo url('ubicaciondelete',[$a->id])?>">Eliminar</a>
                                    </center></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="form-group right">
                    <a href="{{route('ubicacionadd')}}" class="text-white">
                        Adicionar Ubicación
                    </a>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
