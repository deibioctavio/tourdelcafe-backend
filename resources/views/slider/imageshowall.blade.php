@extends('app')

@section('content')
@include('partials.slider.imagedelete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Imágenes del Slider</h2>
            <h4>Listado de imágenes disponibles para el Slider</h4>
            <div class="form-group right">
                    <a href="{{route('sliderimageadd')}}" class="text-white">
                        Adicionar Imagen
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered report">
                    <thead>
                        <tr>
                            <th><center>#</center></th>
                            <th><center>Titulo</center></th>
                            <th><center>Subtitulo</center></th>
                            <th><center>Nombre Original</center></th>
                            <th><center>Nombre Imagen Guardada</center></th>
                            <th><center>Activo</center></th>
                            <th><center>Imagen</center></th>
                            <th colspan="2"><center>Acción</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($sliderImages) < 1)
                            <tr>
                                <td colspan="9">No existen imágenes registradas</td>
                            </tr>
                        @elseif (count($sliderImages) >= 1)
                            @foreach ($sliderImages as $a)
                                <tr>
                                    <td>{{ $a->id }}</td>
                                    <td>{{ $a->titulo }}</td>
                                    <td>{{ $a->subtitulo }}</td>
                                    <td>{{ $a->nombre_imagen_original }}</td>
                                    <td>{{ $a->nombre_imagen_guardada }}</td>
                                    <td><center>
                                        <?php echo ($a->active==1)?"Si":"No"?>
                                    </center></td>
                                    <td><center>
                                        <img src="{{ env('BASE_PUBLIC_URL_PATH'). 'images/sliders/'.$a->nombre_imagen_guardada}}" alt="{{ $a->nombre_imagen_original }}" width="128px" height="72"/>
                                    </center></td>
                                    <td><center>
                                    <a href="<?php echo url('sliderimageedit',[$a->id])?>">Actualizar</a>
                                    </center></td>
                                    <td><center>
                                      <a id="imageslider-del" number="<?php echo $a->id?>"  href="<?php echo url('sliderimagedelete',[$a->id])?>">Eliminar</a>
                                    </center></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
