@extends('app')
@section('content')
@include('partials.slider.imageedit')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Imagen del Slider</h2>
            <h4>Diligencie la información de la imagen</h4>

            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('sliderimageedit', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" value="{{ $slider->id }}" name="id">

                <div class="form-group right">
                    <label class="col-sm-4">Tìtulo</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $slider->titulo }}" name="titulo" class="form-control" maxlength="20">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Súbtitulo</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $slider->subtitulo }}" name="subtitulo" class="form-control" maxlength="30">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Estado: (<?php echo $slider->active==1?"Activo":"Inactivo"?>) </label>
                    <?php
                        $selectedActivo =  ($slider->active==1)?"selected":"";
                        $selectedInactivo =  ($slider->active==0)?"selected":"";
                    ?>
                    <div class="col-sm-8">
                        <select name="active" >
                            <option value="1" <?php echo $selectedActivo?>>Activar</option>
                            <option value="0" <?php echo $selectedInactivo?>>Inactivar</option>
                        </select>
                    </div>
                </div>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
