@extends('app')
@section('content')
@include('partials.slider.edit')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Slider</h2>
            <h4>Diligencie la información del Slider</h4>

            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('slideredit', [])}}" method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}" />
              <input type="hidden" value="{{ $slider->id }}" name="id">
              <input type="hidden" value="{{ $slider->id }}" name="images_count" id="images_count">

              <div class="form-group right">
                  <label class="col-sm-4">Título</label>
                  <div class="col-sm-8">
                      <input type="text" value="{{ $slider->titulo }}" name="titulo" class="form-control" maxlength="20">
                  </div>
              </div>
              <div class="form-group right">
                  <label class="col-sm-4">Contenido</label>
                  <div class="col-sm-8">
                      <textarea rows="3" cols="30" id="contenido" name="contenido">{{ $slider->contenido }}</textarea>
                  </div>
              </div>
              <hr class="mt10 mb10">
              <div class="form-group right">
                  <label class="col-sm-8">Total Imágenes Seleccionadas</label>
                  <div class="col-sm-4" id="images_count">##</div>
              </div>
              <hr class="mt10 mb20">
              <div class="table-responsive">
                  <table class="table table-hover table-bordered report">
                      <thead>
                          <tr>
                              <th><center>#</center></th>
                              <th><center>Titulo</center></th>
                              <th><center>Imagen</center></th>
                              <th><center>Incluir en el Slider</center></th>
                          </tr>
                      </thead>
                      <tbody>
                          @if (count($imageSlider) < 1)
                              <tr>
                                  <td colspan="4">No existen imágenes registradas</td>
                              </tr>
                          @elseif (count($imageSlider) >= 1)
                              @foreach ($imageSlider as $a)
                                  <tr>
                                      <td>{{ $a->id }}</td>
                                      <td>{{ $a->titulo }}</td>
                                      <td><center>
                                          <img src="{{ env('BASE_PUBLIC_URL_PATH'). 'images/sliders/'.$a->nombre_imagen_guardada}}" alt="{{ $a->nombre_imagen_original }}" width="128px" height="72"/>
                                      </center></td>
                                      <td><?php
                                        $checked = ($a->belongs_to_slider == 0)?"":"checked";
                                      ?>
                                        <input
                                                type="checkbox"
                                                id="imageslider-add"
                                                name="image_slider[]"
                                                value="{{ $a->id }}"
                                                {{ $checked }}
                                        />
                                      </td>
                                  </tr>
                              @endforeach
                          @endif
                      </tbody>
                  </table>
              </div>
              <div>
                  <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
              </div>
              <hr class="mt10 mb40">
            </form>
        </div>
    </div>
</div>
@endsection
