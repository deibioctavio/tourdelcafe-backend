@extends('app')
@section('content')
{!! Html::script('assets/js/jquery.validate.additional.methods.min.js', array('type' => 'text/javascript')) !!}
@include('partials.slider.imageadd')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Imagen del Slider</h2>
            <h4>Diligencie la información de la imagen</h4>

            {!! Form::open([ 'route' => [ 'sliderimageadd' ], 'files' => true, 'enctype' => 'multipart/form-data',  "method" => "POST", "class" => "form-horizontal",
            "id" => "register-frm","accept-charset" => "UTF-8"]) !!}

                <div class="form-group right">
                    <label class="col-sm-4">Tìtulo</label>
                    <div class="col-sm-8">
                        <input type="text" value="" name="titulo" class="form-control" maxlength="20">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Súbtitulo</label>
                    <div class="col-sm-8">
                        <input type="text" value="" name="subtitulo" class="form-control" maxlength="30">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Seleccione Imágen&nbsp;(JPG 1280x720)</label>
                     <div class="col-sm-8">
                        <input value="Seleccione" type="file" name="image" id="image"/>
                     </div>
                </div>
                <div class="form-group right">&nbsp;</div>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            {!! Form::close() !!}
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
