@extends('app')
@section('content')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Slider</h2>
            <h4>Información del Slider</h4>
            <div class="form-group right">
                <label class="col-sm-3">Título</label>
                <div class="col-sm-5">{{ $slider->titulo }}</div>
            </div>
            <div class="form-group right">
                <label class="col-sm-3">Contenido</label>
                <div class="col-sm-5">{{ $slider->contenido }}</div>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
