<!DOCTYPE html>
<html lang="es">
@include("partials.page.header")
<body>
    @include('partials.page.nav')
    @if (Session::has('errors'))
         <div class="container">
                <div class="alert alert-warning" role="alert">
                <ul>
                    <strong>Ocurrió un error procesando la solicitud : </strong>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        </div>
    @endif
    @yield('content')
    @include("partials.page.footer")
</body>
</html>