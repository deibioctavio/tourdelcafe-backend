@extends('app')
@section('content')
{!! Html::script('assets/js/jquery.validate.additional.methods.min.js', array('type' => 'text/javascript')) !!}
@include('partials.paquetesturisticos.edit')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Imagen del Paquete Turistico</h2>
            <h4>Diligencie la información de la imagen</h4>

            {!! Form::open([ 'route' => [ 'paquetesturisticosedit' ], 'files' => true, 'enctype' => 'multipart/form-data',  "method" => "POST", "class" => "form-horizontal",
            "id" => "register-frm","accept-charset" => "UTF-8"]) !!}
            <input type="hidden" value="{{ $pt->id }}" name="id" class="form-control">

                <div class="form-group right">
                    <label class="col-sm-4">Nombre</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $pt->nombre }}" name="nombre" class="form-control" maxlength="30">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Posición de la Imagen</label>
                    <div class="col-sm-5">
                        <input type="text" value="{{ $pt->posicion }}" name="posicion" class="form-control" maxlength="2" size="2">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Estado: (<?php echo $pt->active==1?"Activo":"Inactivo"?>) </label>
                    <?php
                        $selectedActivo =  ($pt->active==1)?"selected":"";
                        $selectedInactivo =  ($pt->active==0)?"selected":"";
                    ?>
                    <div class="col-sm-8">
                        <select name="active" >
                            <option value="1" <?php echo $selectedActivo?>>Activar</option>
                            <option value="0" <?php echo $selectedInactivo?>>Inactivar</option>
                        </select>
                    </div>
                </div>
                <div class="form-group right">&nbsp;</div>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            {!! Form::close() !!}
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
