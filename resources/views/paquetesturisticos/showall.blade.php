@extends('app')

@section('content')
@include('partials.paquetesturisticos.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Imágenes Paquetes Turísticos</h2>
            <h4>Listado de imágenes disponibles para el Paquete Turístico</h4>
            <div class="form-group right">
                    <a href="{{route('paquetesturisticosadd')}}" class="text-white">
                        Adicionar Imagen
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered report">
                    <thead>
                        <tr>
                            <th><center>#</center></th>
                            <th><center>Titulo</center></th>
                            <th><center>Nombre Original</center></th>
                            <th><center>Nombre Imagen Guardada</center></th>
                            <th><center>Posición</center></th>
                            <th><center>Activo</center></th>
                            <th><center>Imagen</center></th>
                            <th colspan="2"><center>Acción</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($pt) < 1)
                            <tr>
                                <td colspan="9">No existen imágenes registradas</td>
                            </tr>
                        @elseif (count($pt) >= 1)
                            @foreach ($pt as $a)
                                <tr>
                                    <td>{{ $a->id }}</td>
                                    <td>{{ $a->nombre }}</td>
                                    <td>{{ $a->nombre_imagen_original }}</td>
                                    <td>{{ $a->nombre_imagen_guardada }}</td>
                                    <td>{{ $a->posicion }}</td>
                                    <td><center>
                                        <?php echo ($a->active==1)?"Si":"No"?>
                                    </center></td>
                                    <td><center>
                                        <img src="{{ env('BASE_PUBLIC_URL_PATH'). 'images/promos/'.$a->nombre_imagen_guardada}}" alt="{{ $a->nombre_imagen_original }}" width="128px" height="72"/>
                                    </center></td>
                                    <td><center>
                                        <a href="<?php echo url('paquetesturisticosedit',[$a->id])?>">Actualizar</a>
                                    </center></td>
                                    <td><center>
                                      <a id="paquetesturisticos-del" number="<?php echo $a->id?>"  href="<?php echo url('paquetesturisticosdelete',[$a->id])?>">Eliminar</a>
                                    </center></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
