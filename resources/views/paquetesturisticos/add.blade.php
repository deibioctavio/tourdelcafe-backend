@extends('app')
@section('content')
{!! Html::script('assets/js/jquery.validate.additional.methods.min.js', array('type' => 'text/javascript')) !!}
@include('partials.paquetesturisticos.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Imagen del Paquete Turistico</h2>
            <h4>Diligencie la información de la imagen</h4>

            {!! Form::open([ 'route' => [ 'paquetesturisticosadd' ], 'files' => true, 'enctype' => 'multipart/form-data',  "method" => "POST", "class" => "form-horizontal",
            "id" => "register-frm","accept-charset" => "UTF-8"]) !!}

                <div class="form-group right">
                    <label class="col-sm-4">Nombre</label>
                    <div class="col-sm-8">
                        <input type="text" value="" name="nombre" class="form-control" maxlength="30">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Seleccione Imágen&nbsp;(JPG 1280x720)</label>
                     <div class="col-sm-8">
                        <input value="Seleccione" type="file" name="image" id="image"/>
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Posición de la Imagen</label>
                    <div class="col-sm-5">
                        <input type="text" value="" name="posicion" class="form-control" maxlength="2" size="2">
                    </div>
                </div>
                <div class="form-group right">&nbsp;</div>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            {!! Form::close() !!}
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
