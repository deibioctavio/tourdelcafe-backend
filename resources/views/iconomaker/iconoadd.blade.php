@extends('app')

<?php
    $categorias = array(               
                            'parque_del_cafe',//SOLO
                            'parques_tematicos',
                            'centros_comerciales',
                            'hoteles',
                            'restaurantes',
                            'sitios_interes_museos',
                            'sitios_interes_parques_representativos',
                            'sitios_interes_sitios_religiosos',
                            'cambio_divisa',
                            'cafes_especiales',
                            'hospitales',
                            'medicos_cirujanos',
                            'medicos_ortopedistas',
                            'medicos_cardiologos',
                            'medicos_esteticistas',
                            'medicos_odontologos',
                            'medicos_acupuntura',
                            'medicos_naturistas',
                            'sitios_rumba',
                            'termales',
                            'yipao',
                            'deportes_extremo',
                            'avistamiento_aves',
                            'fiesta_regionales',
                            'chivas_rumberas'
                    );
?>
 
@section('content')
@include('partials.ubicacion.iconoadd')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Iconos (Marker - Google Map)</h2>
            <h4>Diligencie la información del la Ubicación/Marker</h4>    
                            
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('ubicacionadd', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group right"> 
                    <label class="col-sm-3">Latitud</label>
                    <div class="col-sm-9">
                        <input type="text" value="" name="latitud" class="form-control">
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Longitud</label>
                    <div class="col-sm-9">
                        <input type="text" value="" name="longitud" class="form-control">
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre</label>
                    <div class="col-sm-9">
                        <input type="text" value="" name="nombre" class="form-control">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Descripción</label>
                     <div class="col-sm-9">
                        <input type="text" value="" name="descripcion" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Dirección</label>
                     <div class="col-sm-9">
                        <input type="text" value="" name="direccion" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Teléfono</label>
                     <div class="col-sm-9">
                        <input type="text" value="" name="telefono" class="form-control">
                     </div>
                </div>
                <div class="form-group right"> 
                        <label class="col-sm-3">Categoría</label>
                        <div class="col-sm-9">
                            <select name="categoria">
                                <?php
                                    foreach ($categorias as $c) {
                                ?>
                                    <option value="<?php echo $c;?>"><?php echo $c;?></option>
                                <?php

                                    }
                                ?>
                            </select>
                        </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">URL del Ícono</label>
                     <div class="col-sm-9">
                        <input type="text" value="" name="url_icono" class="form-control">
                     </div>
                </div>

                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection