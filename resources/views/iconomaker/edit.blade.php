@extends('app')
@section('content')
@include('partials.iconomaker.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Icono de Marcadores (Marker - Google Map)</h2>
            <h4>Diligencie la información del Icono</h4>

            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('iconomarkeredit', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" value="{{ $iconoMarker->id }}" name="id">

                <div class="form-group right">
                    <label class="col-sm-4">Nombre del icono</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $iconoMarker->nombre }}" name="nombre" class="form-control" maxlength="20">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Estado: (<?php echo $iconoMarker->active==1?"Activo":"Inactivo"?>) </label>
                    <?php
                        $selectedActivo =  ($iconoMarker->active==1)?"selected":"";
                        $selectedInactivo =  ($iconoMarker->active==0)?"selected":"";
                    ?>
                    <div class="col-sm-8">
                        <select name="active" >
                            <option value="1" <?php echo $selectedActivo?>>Activar</option>
                            <option value="0" <?php echo $selectedInactivo?>>Inactivar</option>
                        </select>
                    </div>
                </div>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
