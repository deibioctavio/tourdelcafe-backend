@extends('app') 
@section('content')
{!! Html::script('assets/js/jquery.validate.additional.methods.min.js', array('type' => 'text/javascript')) !!}
@include('partials.iconomaker.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li> 
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Icono de Marcadores (Marker - Google Map)</h2>
            <h4>Diligencie la información del Icono</h4>

            {!! Form::open([ 'route' => [ 'iconomarkeradd' ], 'files' => true, 'enctype' => 'multipart/form-data',  "method" => "POST", "class" => "form-horizontal",
            "id" => "register-frm","accept-charset" => "UTF-8"]) !!}    

                <div class="form-group right"> 
                    <label class="col-sm-4">Nombre del icono</label>
                    <div class="col-sm-8">
                        <input type="text" value="" name="nombre" class="form-control" maxlength="20">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Seleccione Imágen&nbsp;(PNG)</label>
                     <div class="col-sm-8">
                        <input value="Seleccione" type="file" name="image"/>
                     </div>
                </div>
                <div class="form-group right">&nbsp;</div>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            {!! Form::close() !!}
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection