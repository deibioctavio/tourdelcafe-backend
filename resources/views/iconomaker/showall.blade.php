@extends('app')

@section('content')
@include('partials.iconomaker.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Icono de Marcadores (Marker - Google Map)</h2>
            <h4>Listado de iconos para marcadores Registrados</h4>
            <div class="form-group right">
                    <a href="{{route('iconomarkeradd')}}" class="text-white">
                        Adicionar Ícono
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered report">
                    <thead>
                        <tr>
                            <th><center>#</center></th>
                            <th><center>Nombre</center></th>
                            <th><center>Nombre Original</center></th>
                            <th><center>Nombre Imagen GUardada</center></th>
                            <th><center>Activo</center></th>
                            <th><center>Imagen</center></th>
                            <th><center>Acción</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($iconoMarkers) < 1)
                            <tr>
                                <td colspan="8">No existen Iconos registrados</td>
                            </tr>
                        @elseif (count($iconoMarkers) >= 1)
                            @foreach ($iconoMarkers as $a)
                                <tr>
                                    <td>{{ $a->id }}</td>
                                    <td>{{ $a->nombre }}</td>
                                    <td>{{ $a->nombre_imagen_original }}</td>
                                    <td>{{ $a->nombre_imagen_guardada }}</td>
                                    <td><center>
                                        <?php echo ($a->active==1)?"Si":"No"?>
                                    </center></td>
                                    <td><center>
                                        <img src="{{ env('BASE_PUBLIC_URL_PATH'). 'images/markers/'.$a->nombre_imagen_guardada}}" alt="{{ $a->nombre_imagen_original }}">
                                    </center></td>
                                    <td><center>
                                        <a href="<?php echo url('iconomarkeredit',[$a->id])?>">Actualizar</a>
                                    </center></td>
                                    <!--<td><center>
                                    <a id="iconomarker-del" number="<?php echo $a['id']?>"  href="<?php echo url('iconomarkerdelete',[$a->id])?>">Eliminar</a></center></td>-->
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
