@extends('app')

@section('content')
@include('partials.ubicacion.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Listado de Datos de Información</h2>
            <h4>Listado de Datos de Información Registrados</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered report">
                    <thead>
                        <tr>
                            <th><center>#</center></th>
                            <th><center>Titulo</center></th>
                            <th><center>Contenido (Abreviado)</center></th>
                            <th><center>Imágen (Miniatura)</center></th>
                            <th><center>Acción</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($informaciones) < 1)
                            <tr>
                                <td colspan="8">No existen información registrada</td>
                            </tr>
                        @elseif (count($informaciones) >= 1)
                            @foreach ($informaciones as $a)
                                <tr>
                                    <td>{{ $a->id }}</td>
                                    <td>{{ $a->titulo }}</td>
                                    <td><?php
                                        if( strlen($a->contenido) > 200)
                                            echo substr($a->contenido,0,200);
                                        else
                                            echo $a->contenido;
                                    ?></td>
                                    <td><center>
                                        <img src="{{env('BASE_PUBLIC_URL_PATH').'images/informacion/'.$a->nombre_imagen}}" alt="{{$a->nombre_imagen}}" width="128" height="72">
                                    </center></td>
                                    <td><center>
                                    <a href="<?php echo url('informacionedit',[$a->id])?>">Actualizar</a>
                                    </center></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
