@extends('app')
@section('content')
{!! Html::script('assets/js/jquery.validate.additional.methods.min.js', array('type' => 'text/javascript')) !!}
@include('partials.informacion.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Datos de Información</h2>
            <h4>Diligencie la información</h4>

            {!! Form::open([ 'route' => [ 'informacionedit' ], 'files' => true, 'enctype' => 'multipart/form-data',  "method" => "POST", "class" => "form-horizontal",
            "id" => "register-frm","accept-charset" => "UTF-8"]) !!}

                <input type="hidden" value="{{ $informacion->id }}" name="id" class="form-control">
                <input type="hidden" value="n" name="ignore_file" class="form-control">
                <div class="form-group right">
                    <label class="col-sm-4">Título</label>
                    <div class="col-sm-5">
                        <input type="text" value="{{ $informacion->titulo }}" name="titulo" class="form-control">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Contenido</label>
                    <div class="col-sm-5">
                        <textarea name="contenido" id="contenido" rows="3" cols="30">{{ $informacion->contenido }}</textarea>
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">No modificar imagen</label>
                     <div class="col-sm-5">
                        <input value="no-image-edit" type="checkbox" name="image-edit" id="image-edit"/>
                     </div>
                </div>
                <div class="form-group right" id="image_select">
                    <label class="col-sm-4">Seleccione Imágen&nbsp;(JPG)</label>
                     <div class="col-sm-5">
                        <input value="Seleccione" type="file" name="image"/>
                     </div>
                </div>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
