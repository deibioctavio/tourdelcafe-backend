@extends('app') 
@section('content')
@include('partials.ubicacion.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li> 
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Ubicación (Marker - Google Map)</h2>
            <h4>Diligencie la información del la Ubicación/Marker</h4>    
                            
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('ubicacionadd', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group right"> 
                    <label class="col-sm-3">Latitud</label>
                    <div class="col-sm-9">
                        <input type="text" value="" name="latitud" class="form-control">
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Longitud</label>
                    <div class="col-sm-9">
                        <input type="text" value="" name="longitud" class="form-control" maxlength="20">
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre</label>
                    <div class="col-sm-9">
                        <input type="text" value="" name="nombre" class="form-control">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Descripción</label>
                     <div class="col-sm-9">
                        <input type="text" value="" name="descripcion" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Dirección</label>
                     <div class="col-sm-9">
                        <input type="text" value="" name="direccion" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Teléfono</label>
                     <div class="col-sm-9">
                        <input type="text" value="" name="telefono" class="form-control" maxlength="30">
                     </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Categoría</label>
                    <div class="col-sm-9">
                        <select name="categoria_ubicacion_id">
                            @foreach ($categoriasUbicacion as $a)
                                <option value="{{ $a->id }}">{{ $a->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Ícono de la Ubicación</label>
                     <div class="col-sm-9">
                        <select name="icono_marker_id">
                            @foreach ($iconosMarker as $a)
                                <option value="{{ $a->id }}">{{ $a->nombre }}</option>
                            @endforeach
                        </select>
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-5">Mostrar Icono Transporte Público</label>
                     <div class="col-sm-7">
                        <select name="transporte_publico">
                            <option value="1">Si</option>
                            <option value="0">No</option>
                        </select>
                     </div>
                </div>

                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection