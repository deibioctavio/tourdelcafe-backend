@extends('app')
@section('content')
@include('partials.galeriavideos.videosedit')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Galería de Videos</h2>
            <h4>Seleccione los videos que harán parte de la Galaría ( 5 en Total )</h4>

            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('galeriasetvideos', [])}}" method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}" />

              <input type="hidden" value="0" name="videos_count" id="videos_count">

              <div class="form-group right">
                  <label class="col-sm-8">Total Videos Seleccionados</label>
                  <div class="col-sm-4" id="videos_count">##</div>
              </div>
              <hr class="mt10 mb20">
              <div class="table-responsive">
                  <table class="table table-hover table-bordered report">
                      <thead>
                          <tr>
                              <th><center>#</center></th>
                              <th><center>Nombre</center></th>
                              <th><center>Video</center></th>
                              <th><center>Incluir en la Galería</center></th>
                          </tr>
                      </thead>
                      <tbody>
                          @if (count($gv) < 1)
                              <tr>
                                  <td colspan="4">No existen imágenes registradas</td>
                              </tr>
                          @elseif (count($gv) >= 1)
                              @foreach ($gv as $a)
                                  <tr>
                                      <td>{{ $a->id }}</td>
                                      <td>{{ $a->nombre }}</td>
                                      <td><center>
                                        <iframe width="128" height="72"
                                          src='https://www.youtube.com/embed/{{ $a->url_youtube }}?rel=0&showinfo=0'>
                                        </iframe>
                                      </center></td>
                                      <td><?php
                                        $checked = ($a->belongs_to_galery == 0)?"":"checked";
                                      ?>
                                        <input
                                                type="checkbox"
                                                id="videogalery-add"
                                                name="video_galery[]"
                                                value="{{ $a->id }}"
                                                {{ $checked }}
                                        />
                                      </td>
                                  </tr>
                              @endforeach
                          @endif
                      </tbody>
                  </table>
              </div>
              <div>
                  <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
              </div>
              <hr class="mt10 mb40">
            </form>
        </div>
    </div>
</div>
@endsection
