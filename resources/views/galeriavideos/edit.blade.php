@extends('app')
@section('content')
@include('partials.galeriavideos.edit')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Galería de Videos</h2>
            <h4>Diligencie la información del Video</h4>

            {!! Form::open([ 'route' => [ 'galeriavideosedit' ], 'files' => true, 'enctype' => 'multipart/form-data',  "method" => "POST", "class" => "form-horizontal",
            "id" => "register-frm","accept-charset" => "UTF-8"]) !!}
            <input type="hidden" value="{{ $gv->id }}" name="id" class="form-control">

                <div class="form-group right">
                    <label class="col-sm-4">Nombre</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $gv->nombre }}" name="nombre" class="form-control" maxlength="30">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">URL (Youtube)</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $gv->url_youtube }}" name="url_youtube" class="form-control" maxlength="30">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Posición del Video</label>
                    <div class="col-sm-5">
                        <input type="text" value="{{ $gv->posicion }}" name="posicion" class="form-control" maxlength="2" size="2">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Estado: (<?php echo $gv->active==1?"Activo":"Inactivo"?>) </label>
                    <?php
                        $selectedActivo =  ($gv->active==1)?"selected":"";
                        $selectedInactivo =  ($gv->active==0)?"selected":"";
                    ?>
                    <div class="col-sm-8">
                        <select name="active" >
                            <option value="1" <?php echo $selectedActivo?>>Activar</option>
                            <option value="0" <?php echo $selectedInactivo?>>Inactivar</option>
                        </select>
                    </div>
                </div>
                <div class="form-group right">&nbsp;</div>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            {!! Form::close() !!}
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
