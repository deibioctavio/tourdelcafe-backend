@extends('app')

@section('content')
@include('partials.galeriavideos.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Galería de Videos</h2>
            <h4>Listado de videos disponibles</h4>
            <div class="form-group right">
                    <a href="{{route('galeriavideosadd')}}" class="text-white">
                        Adicionar Video
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered report">
                    <thead>
                        <tr>
                            <th><center>#</center></th>
                            <th><center>Nombre</center></th>
                            <th><center>URL del Video (Youtube)</center></th>
                            <th><center>Posición</center></th>
                            <th><center>Activo</center></th>
                            <th><center>Video</center></th>
                            <th colspan="2"><center>Acción</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($gv) < 1)
                            <tr>
                                <td colspan="10">No existen imágenes registradas</td>
                            </tr>
                        @elseif (count($gv) >= 1)
                            @foreach ($gv as $a)
                                <tr>
                                    <td>{{ $a->id }}</td>
                                    <td>{{ $a->nombre }}</td>
                                    <td>{{ $a->url_youtube }}</td>
                                    <td>{{ $a->posicion }}</td>
                                    <td><center>
                                        <?php echo ($a->active==1)?"Si":"No"?>
                                    </center></td>
                                    <td><center>
                                      <iframe width="128" height="72"
                                        src='https://www.youtube.com/embed/{{ $a->url_youtube }}?rel=0&showinfo=0' >
                                      </iframe>
                                    </center></td>
                                    <td><center>
                                        <a href="<?php echo url('galeriavideosedit',[$a->id])?>">Actualizar</a>
                                    </center></td>
                                    <td><center>
                                      <a id="galeriavideos-del" number="<?php echo $a->id?>"  href="<?php echo url('galeriavideosdelete',[$a->id])?>">Eliminar</a>
                                    </center></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
