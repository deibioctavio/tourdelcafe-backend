@extends('app')

@section('navmenu')
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{URL::to('/')}}">
                {!! Html::image('assets/images/logo-tourbotero.jpg', 'Tour Café Botero', array('class' => 'raleway-logo')) !!}
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{URL::to('/')}}">Inicio</a></li>
                @if (Auth::guest())
                    <li><a href="{{route('auth/login')}}">Autenticación</a></li>
                    <li><a href="{{route('auth/register')}}">Registro</a></li>
                @else
                    <li>
                        <a href="{{URL::to('/')}}">{{ Auth::user()->name }}</a>
                    </li>
                    <li><a href="{{route('auth/logout')}}">Salir</a></li>
                    
                @endif
            </ul>
        </div>
    </div>
</nav>
@endsection
 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Registro</h2>
            <h4>Diligencie sus datos de registro</h4> 
            {!! Form::open(['route' => 'auth/register', 'class' => 'form-horizontal']) !!}

                <div class="form-group right">
                    <label class="col-sm-3">Nombre de usuario</label>
                    <div class="col-sm-9">
                        {!! Form::input('text', 'name', '', ['class'=> 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Dirección de correo electrónico</label>
                    <div class="col-sm-9">
                        {!! Form::email('email', '', ['class'=> 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Contraseña</label>
                    <div class="col-sm-9">
                        {!! Form::password('password', ['class'=> 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Confirmar contraseña</label>
                    <div class="col-sm-9">
                        {!! Form::password('password_confirmation', ['class'=> 'form-control']) !!}
                    </div>
                </div>
                <div>
                    {!! Form::submit('enviar',['class' => 'btn btn-rw btn-primary center-block']) !!}
                </div>
            {!! Form::close() !!}
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection