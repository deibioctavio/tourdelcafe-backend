<script type="text/javascript">
$(document).ready(function(){

		$("#register-frm").validate({

	        rules: {
				            nombre: { required: true},
				            image: {
			      						required: true,
			      						extension: "jpg",
			    					},
										posicion: {
											required: true,
											number: true,
											min: 1,
											max: 99
										},
	        },
	        messages: {
	            nombre: {
	                required: "Ingrese el nombre de la imagen para el paquete promocional"
	            },
	            image: {
	                required: "Debe seleccionar una imagen",
	                extension: "Solo son validos imágenes en formato JPG",
									validDimension: "La imagen seleccionada no cumple con las dimensiones esperadas"
	            },
	            posicion: {
	                required: "Ingrese el número de la posición de la imágen",
	                number: "Solo son validos números enteros sin valores decimáles",
									min: "El número debe ser un valor entre 1 y 99",
									max: "El número debe ser un valor entre 1 y 99"
	            },
	        },

	        submitHandler: function(form) {
							var result = imageCheckDimension();
							console.log(result);

							if( result != undefined && result.status == 'OK') {
									form.submit();
							}else{
								alert("Verifique las dimensiones de la imagen ("+result.width+"x"+result.height+")");
								return false;
							}
	        }
		});

		$('input[type=file][name=image]').on('change',
			function(){
				var fileInput = $(this)[0];
			  var file = fileInput.files && fileInput.files[0];

			  if( file ) {

			    var img = new Image();
			    img.src = window.URL.createObjectURL( file );

			    img.onload = function() {
			        _width = img.naturalWidth;
			        _height = img.naturalHeight;
							console.log("_width: " + _width + "_height: " + _height);
			    }
			  }
		});
});
</script>
