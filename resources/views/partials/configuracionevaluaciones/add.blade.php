<script type="text/javascript">
	
	function generateVigenciaComboBox(){

		$.ajax(
		    {
		        type: "POST",
		        url: base_url+'/configuracionevaluacion/getvalidvigencia',
		        data: {
			          	_token: $('input[type=hidden][name=_token]').val(),
			     },
			    beforeSend: function(){
			    	$('#vigencia-container').html(jsAjaxLoadingImageObjectString24);
			    	$('input[type=submit][value=Registrar]').hide();
			    },
		        success: function(response)
		        {
		        	var select = '<select id="vigencia" name="vigencia">';
		        	
		        	$.each(response, function( index, element ) {
						  select += '<option value="'+element.vigencia+'">'+element.vigencia+'</option>';
					});

		        	select += '</select>';

		        	$('#vigencia-container').html(select);
		        	$('input[type=submit][value=Registrar]').show();

		        },
		        error: function(xhr, textStatus, errorThrown)
		        {
		            console.error(base_url+'/configuracionevaluacion/getvalidvigencia');
		            alert('error cargando el ajax');
		            return false;
		        }
		    });
	}


	$(document).ready(function(){

		jQuery.validator.addMethod("validate100p", 
        
            function(value,element,params) {

            	//console.log(params);

            	var total = 0.00;

            	//var parent = $(element).parents("form[id=register-frm]");
            	var elements = $('input[item=percen-value]').parent().parent().parent().find('input[item=percen-value]');
        		
        		//console.log( "Elements Count: " + elements.length );

        		elements.each(

        			function(index)
        			{
						var val = isNaN( parseFloat( $(this).val() ) )?0:parseFloat( $(this).val() );
						total += val;
						//console.log($(this).attr("id") + " " + val);
            		}
            	);

                //console.log("Total: " + total);
                return total == parseFloat( params );
                //return false;
            }, 
            
            "La suma del valor de los elementos debe se igual a 100.00"
        );

		jQuery.validator.addMethod("validateVigencia", 

        	function(value,element,params)
			{
			    $.ajax(
			    {
			        type: "POST",
			        url: base_url+'/configuracionevaluacion/validatevigencia',
			        data: {
				          	vigencia: params,
				          	_token: $('input[type=hidden][name=_token]').val(),
				     },
			        success: function(response)
			        {
			        	console.log(params);
			            
			            if (eval(response))
			            {
			              console.error("ENTRO TRUE");
			              return true;
			            }
			            else
			            {
			               console.error("ENTRO FALSE");
			               return false;
			            }
			        },
			        error: function(xhr, textStatus, errorThrown)
			        {
			            alert('error cargando el ajax '+url + query);
			            return false;
			        }
			    });

		}, "ya se encuentra registradala configuración para el año seleccionado");
        
		
		$("#register-frm").validate({

	        rules: {

	            n_compromisos_laborales: 
					{ 
						required: true,
      					digits: true,
						min: 1,
						max: 50
					},

	            valor_compromisos_laborales: 
	            	{ 
	            		required: true,
	            		decimal_number: true,
	            		min: 0,
	            		max: 100
	            	},

	            n_competencias_comportamentales:
	            	{
						required: true,
						min: 1,
						max: 50
					},

				valor_competencias_comportamentales: 
	            	{ 
	            		required: true,
	            		decimal_number: true,
	            		min: 0,
	            		max: 100
	            	},
	        },
	        
	        // Specify the validation error messages
	        messages: {
	           
	            n_compromisos_laborales: {
	                required: "Ingrese una cantidad válida de compromisos",
	                digits: "Ingrese solo valores numéricos mayores a cero (0), sin punto decimal!",
	                min: "Ingrese un número positivo mayor que cero",
	                max: "El valor ingresado supera el máximo permitido (50)"

	            },
	            valor_compromisos_laborales: {
	                required: "Ingrese un valor porcentual válido",
	                min: "Ingrese un número positivo mayor que cero",
	                max: "El valor ingresado supera el máximo permitido (100.00)"
	            },
	            n_competencias_comportamentales: {
	                required: "Ingrese una cantidad válida de competencias",
	                digits: "Ingrese solo valores numéricos mayores a cero (0), sin punto decimal!",
	                min: "Ingrese un número positivo mayor que cero",
	                max: "El valor ingresado supera el máximo permitido (50)"
	            },
	            valor_competencias_comportamentales: {
	                required: "Ingrese un valor numérico válido"
	            }
	        },
	        submitHandler: function(form) {
	            form.submit();
	        }			
		});

		$('input[name=valor_compromisos_laborales]').rules('add',{validate100p:100});
		//$('select[name=vigencia]').rules("add", {max:120});

		generateVigenciaComboBox();
	});
</script>