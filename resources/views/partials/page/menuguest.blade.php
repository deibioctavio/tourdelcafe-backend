<li id="crud_admin" class="dropdown dropdown-main">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        Administrar
        <span class="caret"></span>
    </a>
        <ul class="dropdown-menu dropdown-subhover dropdown-animation animated fadeIn">
            <li class="dropdown dropdown-submenu">
                <a class="trigger">Ubicaciones (Markers)</a>
                <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
                    <li>
                        <a tabindex="-1" href="{{route('iconomarkeradd')}}">
                            Adicionar Icono Marker
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="{{route('iconomarkershowall')}}">
                            Ver todos los Iconos
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="{{route('ubicacionadd')}}">
                            Adicionar Ubicación
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="{{route('ubicacionshowall')}}">
                            Ver todas las  Ubicaciones
                        </a>
                    </li>
                </ul>
            </li>
            <li class="dropdown dropdown-submenu">
                <a class="trigger">Slider</a>
                <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
                  <li>
                      <a tabindex="-1" href="{{route('slideredit')}}">
                          Actualizar Información Slider
                      </a>
                  </li>
                    <li>
                        <a tabindex="-1" href="{{route('sliderimageadd')}}">
                            Adicionar Imagen Slider
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="{{route('sliderimageshowall')}}">
                            Ver todos las Imágenes
                        </a>
                    </li>
                </ul>
            </li>
            <li class="dropdown dropdown-submenu">
                <a class="trigger">Paquetes Turísticos</a>
                <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
                    <li>
                        <a tabindex="-1" href="{{route('paquetesturisticosadd')}}">
                            Adicionar Imagen
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="{{route('paquetesturisticosshowall')}}">
                            Ver todos las Imágenes
                        </a>
                    </li>
                </ul>
            </li>
            <li><a href="{{route('informacioneditshowall')}}">Información</a></li>
            <li class="dropdown dropdown-submenu">
                <a class="trigger">Galería de Imágenes</a>
                <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
                  <li>
                      <a tabindex="-1" href="{{route('galeriasetimagenes')}}">
                          Seleccionar Imágenes Galería
                      </a>
                  </li>
                    <li>
                        <a tabindex="-1" href="{{route('galeriaimagenesadd')}}">
                            Adicionar Imagen
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="{{route('galeriaimagenesshowall')}}">
                            Ver todos las Imágenes
                        </a>
                    </li>
                </ul>
            </li>
            <li class="dropdown dropdown-submenu">
                <a class="trigger">Galería de Videos</a>
                <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
                  <li>
                      <a tabindex="-1" href="{{route('galeriasetvideos')}}">
                          Seleccionar Videos Galería
                      </a>
                  </li>
                    <li>
                        <a tabindex="-1" href="{{route('galeriavideosadd')}}">
                            Adicionar Video
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="{{route('galeriavideosshowall')}}">
                            Ver todos los Videos
                        </a>
                    </li>
                </ul>
            </li>
            <li class="dropdown dropdown-submenu">
                <a class="trigger">Perfiles</a>
                <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
                    <li>
                        <a tabindex="-1" href="{{route('userroleshowall')}}">
                            Ver Usuarios y Roles
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{route('auth/register')}}"> Registrar usuarios </a>
            </li>
        </ul>
</li>
