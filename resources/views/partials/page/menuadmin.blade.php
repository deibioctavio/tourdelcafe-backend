<li id="crud_admin" class="dropdown dropdown-main">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        Administrar
        <span class="caret"></span>
    </a>
        <ul class="dropdown-menu dropdown-subhover dropdown-animation animated fadeIn">
            <?php
                $routes = getRoutesByUserId(Auth::user()->id);
                $tmp = array();
                $i = 0;
                foreach ($routes as $r) {
                    
                    if($r->menu_slug == 'admin'){
                        $tmp[$r->group][$i]['route'] = $r->route;
                         $tmp[$r->group][$i]['description'] = $r->description;
                         $i++;
                    }
                }

                foreach ($tmp as $k => $v){
            ?>
                <li class="dropdown dropdown-submenu">
                    <a class="trigger"><?php echo ucfirst($k);?></a>
                    <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
            <?php
                    foreach ($v as $element){
                        
                        $url = route($element['route']);
            ?>
                        <li>
                            <a tabindex="-1" href="{{ $url }}">{{ $element['description'] }}</a>
                        </li>
            <?php
                    }
            ?>
                    </ul>                                       
                </li> 
            <?php        
                    
                }
            ?>
            <li>
                <a href="{{route('auth/register')}}"> Registra usuarios </a>
            </li>                             
        </ul>
</li>