<script type="text/javascript">
	$(document).ready(function(){
		
		$("#register-frm").validate({

	        rules: {
	            
	            description: { required: true}
	        },
	        
	        // Specify the validation error messages
	        messages: {
	           
	            description: {
	                required: "Ingrese el detalle del compromiso"
	            }
	        },
	        
	        submitHandler: function(form) {
	            form.submit();
	        }			
		});
	});
</script>