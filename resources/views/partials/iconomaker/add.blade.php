<script type="text/javascript">
	$(document).ready(function(){

		$("#register-frm").validate({

	        rules: {

	            nombre: { required: true},
	            image: {
      						required: true,
      						extension: "png"
    			}
	        },

	        // Specify the validation error messages
	        messages: {

	            nombre: {
	                required: "Ingrese el nombre del icono para el marker"
	            },

	            image: {
	                required: "Debe seleccionar una imagen",
	                extension: "Solo son validos imágenes en formato PNG"
	            },
	        },

	        submitHandler: function(form) {
	            form.submit();
	        }
		});
	});
</script>
