<script type="text/javascript">
	$(document).ready(function(){
		
		$("#register-frm").validate({

	        rules: {
	            
	            latitud: { required: true},
	            longitud: { required: true},
	            nombre: { required: true},
	            descripcion: { required: true},
	            url_icono: { required: true},
	        },
	        
	        // Specify the validation error messages
	        messages: {
	           
	            nombre: {
	                required: "Ingrese el nombre de la ubicación/marker"
	            },
	            latitud:{
	            	required: "Este campo es requerido"	
	            },
	            longitud:{
	            	required: "Este campo es requerido"	
	            },
	            descripcion:{
	            	required: "Este campo es requerido"	
	            },
	            url_icono:{
	            	required: "Este campo es requerido"	
	            }
	        },
	        
	        submitHandler: function(form) {
	            form.submit();
	        }			
		});
	});
</script>