<script type="text/javascript">
	$(document).ready(function(){

		$("#register-frm").validate({

	        rules: {

	            titulo: { required: true},
	            contenido: { required: true},
	            image: {
      						required: true,
      						extension: "jpg"
    				}
	        },

	        // Specify the validation error messages
	        messages: {

	            titulo: {
	                required: "Ingrese el titulo de la información"
	            },

	            image: {
	                required: "Debe seleccionar una imagen",
	                extension: "Solo son validos imágenes en formato JPG"
	            },
	        },

					submitHandler: function(form) {

							if( $('input[type=checkbox]').is(':checked') ){

								$('input[type=hidden][name=ignore_file]').val('s');
								form.submit();
							}else{

									var result = imageCheckDimension();

									if( result != undefined && result.status == 'OK') {
											form.submit();
									}else{
										alert("Verifique las dimensiones de la imagen ("+result.width+"x"+result.height+")");
										return false;
									}
							}

	        }
		});

		$('input[type=checkbox]').on('click',function(){

				if( $(this).is(':checked') == true ){
					$('div#image_select').hide();
					$( "input[type=file][name=image]").rules( "remove", "extension required" );
					$('input[type=hidden][name=ignore_file]').val('s');
				}else{
					$('div#image_select').show();
					$( "input[type=file][name=image]").rules( "add", "extension required" );
					$('input[type=hidden][name=ignore_file]').val('n');
				}
		});

		$('input[type=file][name=image]').on('change',
			function(){
				var fileInput = $(this)[0];
			  var file = fileInput.files && fileInput.files[0];

			  if( file ) {

			    var img = new Image();
			    img.src = window.URL.createObjectURL( file );

			    img.onload = function() {
			        _width = img.naturalWidth;
			        _height = img.naturalHeight;
							console.log("_width: " + _width + "_height: " + _height);
			    }
			  }
		});

	});
</script>
