<script type="text/javascript">
	$(document).ready(function(){
		
		$( "#birthday" ).datepicker({ dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true,yearRange:'-99:+0'});
		$("#register-frm").validate({

	        rules: {
	            
	            name: { required: true},
	            lastname: { required: true},
	            document_number: { required: true},
	            name: { required: true},
	            name: { required: true},
	        },	      
	        messages: {
	           
	            name: {
	                required: "Ingrese el nombre"
	            },
	            lastname: {
	                required: "Ingrese el apellido"
	            },
	            document_number: {
	                required: "Ingrese el número de documento"
	            }
	        },
	        
	        submitHandler: function(form) {
	            form.submit();
	        }			
		});
	});
</script>
<script type="text/javascript">
    $(document).ready(function(){

		
		$('select[name=nivel_id][id=nivel_id]').on("change",recargar_listado_niveles);

		recargar_listado_niveles();
    });

    function recargar_listado_niveles(){

    	var newSelectedNivel = $('select[name=nivel_id][id=nivel_id]').val();

    	$.ajax({
		                url:base_url+'/cargoslistjson/'+newSelectedNivel,
		                dataType:'json',
		                async:false,
		                type:'get',
		                processData: false,
		                contentType: false,
		                success:function(response){
		                  var selectHTML = "";
		                  selectHTML += '<select id="cargo_id" name="cargo_id">';

							$.each(response, function(index,element) {
								
								selectedCargo = $('input[name=selectedCargo][id=selectedCargo]').val();
								selectedString = (selectedCargo == element.id)?"selected":"";
								selectHTML += '<option value="'+element.id+'" '+selectedString+'>'+element.name+'</option>';
							});

		                  selectHTML += "</select>";

		                  $("div[id=cargo-list-container]").html(selectHTML);
		                },
		    });
    }
</script>