<script type="text/javascript">

$(document).ready(function(){

		$("#register-frm").validate({

	        rules: {
				            titulo: { required: true},
										subtitulo: { required: true}
	        },
	        messages: {
	            titulo: {
	                required: "Ingrese el titulo de la imagen para el slider"
	            },
							subtitulo: {
	                required: "Ingrese el subtitulo de la imagen para el slider"
	            },
	        },

	        submitHandler: function(form) {
						form.submit();
	        }
		});
});
</script>
