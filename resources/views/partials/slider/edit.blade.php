<script type="text/javascript">

$(document).ready(function(){

		$("#register-frm").validate({

	        rules: {
				            titulo: { required: true},
										contenido: { required: true}
	        },
	        messages: {
	            titulo: {
	                required: "Ingrese el titulo del slider"
	            },
							contenido: {
	                required: "Ingrese la información del contenido del slider"
	            }
	        },

	        submitHandler: function(form) {

						if( $('input#images_count').val() == _sliderImageAmount ){
							form.submit();
						}else{
							alert("El número de imágenes seleccionadas ("+$('input#images_count').val()+") debe ser igual a: " + _sliderImageAmount);
							return false;
						}
	        }
		});

		$('input[type=checkbox]').each(function(){
  		$(this).on('click',function(){
      	$('input#images_count').val($('input[type=checkbox]:checked').length);
				$('div#images_count').html($('input[type=checkbox]:checked').length);

				if( $('input#images_count').val() == _sliderImageAmount ){
					$('div#images_count').removeClass('red');
					$('div#images_count').addClass('green');

				}else{
					$('div#images_count').addClass('red');
					$('div#images_count').removeClass('green');
				}
  		});
		});

		$('input#images_count').val($('input[type=checkbox]:checked').length);
		$('div#images_count').html($('input[type=checkbox]:checked').length);

		if( $('input#images_count').val() == _sliderImageAmount ){
			$('div#images_count').addClass('green');
		}else{
			$('div#images_count').addClass('red');
		}
});
</script>
