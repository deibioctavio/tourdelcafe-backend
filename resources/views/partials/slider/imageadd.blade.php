<script type="text/javascript">
$(document).ready(function(){

		$("#register-frm").validate({

	        rules: {
				            titulo: { required: true},
										subtitulo: { required: true},
				            image: {
			      						required: true,
			      						extension: "jpg"
			    					}
	        },
	        messages: {
	            titulo: {
	                required: "Ingrese el titulo de la imagen para el slider"
	            },
							subtitulo: {
	                required: "Ingrese el subtitulo de la imagen para el slider"
	            },
	            image: {
	                required: "Debe seleccionar una imagen",
	                extension: "Solo son validos imágenes en formato JPG",
									validDimension: "La imagen seleccionada no cumple con las dimensiones esperadas"
	            },
	        },

	        submitHandler: function(form) {
							var result = imageCheckDimension();
							console.log(result);

							if( result != undefined && result.status == 'OK') {
									form.submit();
							}else{
								alert("Verifique las dimensiones de la imagen ("+result.width+"x"+result.height+")");
								return false;
							}
	        }
		});

		$('input[type=file][name=image]').on('change',
			function(){
				var fileInput = $(this)[0];
			  var file = fileInput.files && fileInput.files[0];

			  if( file ) {

			    var img = new Image();
			    img.src = window.URL.createObjectURL( file );

			    img.onload = function() {
			        _width = img.naturalWidth;
			        _height = img.naturalHeight;
							console.log("_width: " + _width + "_height: " + _height);
			    }
			  }
		});
});
</script>
