<script type="text/javascript">

function showActionLink(evaluacionId){
	$.ajax({
	  			url: base_url+"/evaluaciones/getcompromisoslaboralesbyevaluacionidajax/"+evaluacionId,
	  			dataType: "json",
	  			data: {},
	  			type: "get",
	      		success: function( data ) {

	      			//console.log(evaluacionId + " - " + data.length);

	      			$('img[id=evaluacion-'+evaluacionId+']').addClass("hidden");
	      			if(data.length <= 0){

	      				$('a[id=evaluacion-add][evaluacion='+evaluacionId+']').removeClass("hidden");

	      			}else{
	      				$('a[id=evaluacion-show][evaluacion='+evaluacionId+']').removeClass("hidden");
	      			}
	      		}
        	});
}
$(document).ready(function(){

	$('a[id=evaluacion-add]').each(function(index,element){
		var evaluacionId = $(element).attr('evaluacion');
		showActionLink(evaluacionId);
	});
});
</script>