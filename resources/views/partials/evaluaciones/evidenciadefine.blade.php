<script type="text/javascript">

	var rowCount = 0;

	function calculateTotalCompromiso(){

		var total = 0;

		$('select[id^=percentual_weight_]').each(function(){
			total += parseInt($(this).val());
		});

		$('input[id=total_registrado]').val(total);

	}

	$(document).ready(function(){

		$("#register-frm").validate({

	        rules: {

	            total_registrado: 
					{ 
						required: true,
						equalTo: "#total_pactado"
					}
	        },
	        
	        // Specify the validation error messages
	        messages: {
	           
	            total_registrado: {
	                required: "Valor requerido",
	                equalTo: "El total de los valores de las evidencias debe ser igual al total pactado"
	            }
	        },
	        submitHandler: function(form) {
	            form.submit();
	        }			
		});

		$('a[id=evidencia-define-0]').on('click', function(e) {
        	e.preventDefault();

        	rowCount++;
        	var row = $('tr[id=row-evidencia-0]').clone();

        	row.attr('id','row-evidencia-'+rowCount);
        	row.find('input').attr('id','description_'+rowCount);
        	row.find('select').attr('id','percentual_weight_'+rowCount);
        	row.find('textarea').attr('id','comments_'+rowCount);
        	row.find('a[id=evidencia-define-0]').html('<b>Remover</b>');
        	row.find('a[id=evidencia-define-0]').attr('id','evidencia-define-'+rowCount);
        	row.find('a[id=evidencia-define-'+rowCount+']').attr('row-id','row-evidencia-'+rowCount);
        	row.appendTo('#tabla-evidencias tbody');

        	$('a[id=evidencia-define-'+rowCount+']').bind('click',function(e){
        		e.preventDefault();
        		$("#tabla-evidencias tbody").find('tr#'+$(this).attr('row-id')).remove();
        		calculateTotalCompromiso();
        	})

        	$('select[id=percentual_weight_'+rowCount+']').bind('change',function(){
        		calculateTotalCompromiso();
        	})

        	calculateTotalCompromiso();
     	});

     	$('select[id=percentual_weight_0]').on('change',function(){
     		calculateTotalCompromiso();
     	});

     	calculateTotalCompromiso();
	});
</script>