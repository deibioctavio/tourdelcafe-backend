<script type="text/javascript">

$(document).ready(function(){

		$("#register-frm").validate({

	        rules: {
				            titulo: { required: true},
	        },
	        messages: {
	            titulo: {
	                required: "Ingrese el titulo del slider"
	            }
	        },

	        submitHandler: function(form) {

						if( $('input#videos_count').val() == _videoGaleryAmount ){
							form.submit();
						}else{
							alert("El número de videos seleccionados ("+$('input#videos_count').val()+") debe ser igual a: " + _videoGaleryAmount);
							return false;
						}
	        }
		});

		$('input[type=checkbox]').each(function(){
  		$(this).on('click',function(){
      	$('input#videos_count').val($('input[type=checkbox]:checked').length);
				$('div#videos_count').html($('input[type=checkbox]:checked').length);

				if( $('input#videos_count').val() == _videoGaleryAmount ){
					$('div#videos_count').removeClass('red');
					$('div#videos_count').addClass('green');

				}else{
					$('div#videos_count').addClass('red');
					$('div#videos_count').removeClass('green');
				}
  		});
		});

		$('input#videos_count').val($('input[type=checkbox]:checked').length);
		$('div#videos_count').html($('input[type=checkbox]:checked').length);

		if( $('input#videos_count').val() == _videoGaleryAmount ){
			$('div#videos_count').addClass('green');
		}else{
			$('div#videos_count').addClass('red');
		}
});
</script>
