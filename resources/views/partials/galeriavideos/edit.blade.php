<script type="text/javascript">
$(document).ready(function(){

		$("#register-frm").validate({

	        rules: {
				            nombre: { required: true},
										url_youtube: {
			      						required: true
			    					},
										posicion: {
											required: true,
											number: true,
											min: 1,
											max: 99
										},
	        },
	        messages: {
	            nombre: {
	                required: "Ingrese el nombre de la imagen para el paquete promocional"
	            },
	            url_youtube: {
	                required: "Ingrese la URL del video"
	            },
	            posicion: {
	                required: "Ingrese el número de la posición de la imágen",
	                number: "Solo son validos números enteros sin valores decimáles",
									min: "El número debe ser un valor entre 1 y 99",
									max: "El número debe ser un valor entre 1 y 99"
	            },
	        },

	        submitHandler: function(form) {

						form.submit();
	        }
		});
});
</script>
