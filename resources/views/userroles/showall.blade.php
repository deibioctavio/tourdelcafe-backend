@extends('app')
 
@section('content')
@include('partials.proyectos.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Roles - Usuario</h2>
            <h4>Listado de Usuario y Sus Roles</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre de usuario</th>
                            <th>Correo Electrónico</th>
                            <th>Rol</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $i = 1;
                        ?>
                            @foreach ($userRoles as $ur)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $ur['user_name'] }}</td>
                                    <td>{{ $ur['user_email']}}</td>
                                    <td><?php echo isset($ur['user_roles'])?$ur['user_roles']['role_name']:"Sin rol asignado";?>
                                    </td>
                                    <td>
                                        <a href="<?php echo url('userroleedit',[$ur['user_id']])?>">Actualizar</a></td>
                                    </td>
                                    
                                </tr>
                            <?php $i++?>                                 
                            @endforeach
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection