@extends('app')
 
@section('content')
@include('partials.areas.add')
@include('partials.areas.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Rol - Usuario</h2>
            <h4>Diligencie la información del Rol</h4>    
                            
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('userroleedit', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="user_id" value="{{ $user->id }}">
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre de usuario</label>
                    <div class="col-sm-9">
                         <input type="text" readonly value="{{ $user->name }}">
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Correo electónico</label>
                    <div class="col-sm-9">
                        <input type="text" readonly value="{{ $user->email }}">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Seleccione el Rol</label>
                     <div class="col-sm-9">
                        <select name="rol_id">
                            <option value="0">Ninguno</option>
                            @foreach ($roles as $r)
                                <option value="{{ $r->id }}" <?php echo ($r->id == $usuarioRol['id'])?"selected":"";?>>{{ $r->name }}</option>
                            @endforeach
                        </select>
                     </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection