@extends('app')
 
@section('content')
 <div class="container">
    <div class="row">
       <br />
            @include('partials.messages')
      <hr class="mt10 mb40">
        <h1>Bienvenidos a Tour del Café Botero</h1>
        <hr class="mt10 mb40 invisible">
        <p class="lead">
            Tour Café Botero
        </p>
        <p>
            Plataforma de Administración de Datos de la App de Tour Café Botero.
        </p>

        <hr class="mt10 mb10">
        <!--
        @for ($i = 10; $i < 5; $i++)
            @include('partials.fileuploadajax',
                                                array(
                                                        "formId" => $i,
                                                        "uploadControllerRoute" => "addajax",
                                                        "uploadLinkName" => "Subir Archivo",
                                                        "hiddemForm" => true
                                                    )
                    )
        @endfor
        -->
    </div>
</div>
@endsection