<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'sliders';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   *
   *
   */
  protected $fillable = [
                          'titulo',
                          'contenido'
                        ];

  public function images() {
      return $this->hasMany('App\ImageSlider');
  }

}
