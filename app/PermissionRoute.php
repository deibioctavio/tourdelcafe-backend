<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionRoute extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permission_route';

    protected $fillable = ['slug','route','menu_slug','menu_visible','description'];
}
