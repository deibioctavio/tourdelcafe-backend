<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaInformacion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categoria_informacions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     *
     */
    protected $fillable = [
    						'nombre',
                            'slug',
    						'active'];

    public function informaciones() {
        
       return $this->hasMany('App\Informacion');
    }
}
