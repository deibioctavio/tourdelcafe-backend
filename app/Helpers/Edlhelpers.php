<?php
	
	use Illuminate\Config\Repository;
	use App\RoleUser;
	use App\PermissionRole;
	use App\PermissionRoute;

	use Illuminate\Auth\AuthManager;

	function getRealId($fakeId){

		if($fakeId - Config::get('app.idplusvar') <= 0)
			return 0;
		
		return $fakeId - Config::get('app.idplusvar');
	}

	function getTimestamp(){
		$date = new DateTime();
        return  $date->getTimestamp();
	}

	function getRoutesByUserId($userId = NULL){

		$routes = array();

		if( $userId != NULL && is_int($userId) && $userId > 0 ){

			$roles = RoleUser::where('user_id', $userId)->get();

			$permissions = array();

			if( $roles ){

				foreach($roles as $r){

					$permissionRole = PermissionRole::where('role_id',$r->role_id)->get();

					foreach ($permissionRole as $pr) {
						$permissions[$pr->permission_id] = $pr->permission_id;
					}	
				}
			}

			$prm = PermissionRoute::whereIn('permission_id',array_keys($permissions))->get();
		}

		return $prm;
	}