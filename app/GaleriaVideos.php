<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GaleriaVideos extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'galeria_videos';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   *
   *
   */
  protected $fillable = [
                          'nombre',
                          'url_youtube',
                          'posicion',
                          'active',
                          'belongs_to_galery'
                        ];
}
