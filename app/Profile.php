<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    protected $fillable = [
                            'name','lastname','document_type','document_number',
                            'birthday','address','gender','phone','cellphone'
                        ];

    public function user() {
        
       return $this->belongsTo('App\User');
    }
}
