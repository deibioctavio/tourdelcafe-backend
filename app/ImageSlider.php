<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageSlider extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'image_sliders';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   *
   *
   */
  protected $fillable = [
                          'nombre',
                          'nombre_imagen_original',
                          'nombre_imagen_guardada',
                          'active',
                          'belongs_to_slider'
                        ];

  public function slider() {
      return $this->belongsTo('App\Slider');
  }
}
