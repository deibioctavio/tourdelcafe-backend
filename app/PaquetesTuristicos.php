<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaquetesTuristicos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'paquetes_turisticos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     *
     */
    protected $fillable = [
                            'nombre',
                            'nombre_imagen_original',
                            'nombre_imagen_guardada',
                            'posicion',
                            'active'
                          ];
}
