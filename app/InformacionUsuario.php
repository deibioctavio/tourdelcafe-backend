<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformacionUsuario extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'informacion_usuarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     */
    protected $fillable = [
    						'nombre',
    						'correo_electronico',
    						'fecha_nacimiento',
    						'telefono'];
}
