<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PaquetesTuristicos;
use Intervention\Image\Facades\Image;

class PaqueteTuristicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $pt = PaquetesTuristicos::orderBy('id','asc')->get();
      return \View::make('paquetesturisticos.showall',

                          array(
                                  'pt' => $pt,
                                  'isReportView'=>true
                              )
                      );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('paquetesturisticos.add',array());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $image  = $request->file('image');

      $imageName = "";

      try
      {
          $extension      =   $image->getClientOriginalExtension();
          $imageRealPath  =   $image->getRealPath();
          $thumbName      =   'promo-image-'.date('YmdHis').".".$extension;

          $img = Image::make($imageRealPath);
          $img->resize(env('IMAGE_SIZE_WIDTH'),env('IMAGE_SIZE_HEIGHT'));

          $imageSaved = $img->save(env('BASE_PUBLIC_DIR_PATH'). 'images/promos/'. $thumbName);

          if( $imageSaved ){

              $imageName = $imageSaved->basename;

              $pt = new PaquetesTuristicos();
              $pt->nombre = $request->input('nombre');
              $pt->nombre_imagen_original = $image->getClientOriginalName();
              $pt->nombre_imagen_guardada = $thumbName;
              $pt->posicion = $request->input('posicion');

              if(!$pt->save()){

                  return redirect()->back()->withError('Error guardando la información de la imagen en la base de datos');

              }else{

                  $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
                  return redirect('paquetesturisticosshowall');
              }

          }else{

              return redirect()->back()->withError('Error procesando la imagen');
          }
      }
      catch(Exception $e)
      {
          return $e;
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $pt = PaquetesTuristicos::where('id',$id)->first();
      return \View::make('paquetesturisticos.edit',array('pt' => $pt));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $pt = PaquetesTuristicos::find($id);
        $pt->nombre = $request->input('nombre');
        $pt->posicion = $request->input('posicion');
        $pt->active = $request->input('active');

        if(!$pt->save()){

            return redirect()->back()->withError('Error guardando la información de la ubicación en la base de datos');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('paquetesturisticosshowall');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $pt = PaquetesTuristicos::find($id);

      if(file_exists( env('BASE_PUBLIC_DIR_PATH').'images/promos/'.$pt->nombre_imagen_guardada)){

          if(unlink(env('BASE_PUBLIC_DIR_PATH').'images/promos/'.$pt->nombre_imagen_guardada)){

              $pt->delete();
              return redirect('paquetesturisticosshowall');

          }else{

              return redirect()->back()->withError('Error Eliminando la imágen');
          }
      }else{
          return redirect()->back()->withError('Error la imagen no existe');
      }
    }

    public function getImages()
    {
      $header = array (
              'Content-Type' => 'application/json; charset=UTF-8',
              'charset' => 'utf-8'
          );

      $paquetes = PaquetesTuristicos::where('active',1)
                      ->orderBy('posicion','asc')
                      ->orderBy('id','asc')
                      ->get();

      $i = 1;
      $imagesPaquetesTuristicos = array();

      foreach ($paquetes as $p) {

        $imagesPaquetesTuristicos[$i]['imagename'] = $p->nombre_imagen_guardada;
        $i++;
      }

      return response()->json($imagesPaquetesTuristicos , 200, $header, JSON_UNESCAPED_UNICODE);
    }
}
