<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Slider;
use App\ImageSlider;
use DB;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageIndex()
    {
      $sliderImages = ImageSlider::get();

      return \View::make('slider.imageshowall',

                          array(
                                  'sliderImages' => $sliderImages,
                                  'isReportView'=>true
                              )
                      );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageCreate()
    {
        return \View::make('slider.imageadd',array());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function imageStore(Request $request)
    {
      $image  = $request->file('image');

      $imageName = "";

      try
      {
          $extension      =   $image->getClientOriginalExtension();
          $imageRealPath  =   $image->getRealPath();
          $thumbName      =   'slider-image-'.date('YmdHis').".".$extension;

          $img = Image::make($imageRealPath);
          $img->resize(env('IMAGE_SIZE_WIDTH'),env('IMAGE_SIZE_HEIGHT'));

          $imageSaved = $img->save(env('BASE_PUBLIC_DIR_PATH'). 'images/sliders/'. $thumbName);

          if( $imageSaved ){

              $imageName = $imageSaved->basename;

              $imageSlider = new ImageSlider();
              $slider = Slider::find(1);
              $imageSlider->titulo = $request->input('titulo');
              $imageSlider->subtitulo = $request->input('subtitulo');
              $imageSlider->nombre_imagen_original = $image->getClientOriginalName();
              $imageSlider->nombre_imagen_guardada = $thumbName;
              $imageSlider->slider_id = $slider->id;

              if(!$imageSlider->save()){

                  return redirect()->back()->withError('Error guardando la información de la imagen en la base de datos');

              }else{

                  $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
                  return redirect('sliderimageshowall');
              }

          }else{

              return redirect()->back()->withError('Error procesando la imagen');
          }
      }
      catch(Exception $e)
      {
          return $e;
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = 1)
    {
      $slider = Slider::where('id',$id)->first();
      return \View::make('slider.show',array('slider'=>$slider));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = 1)
    {
      $slider = Slider::where('id',$id)->first();
      $imageSlider = ImageSlider::where('active', 1)->orderBy('id', 'asc')->get();
      return \View::make('slider.edit',array(
                                              'slider' => $slider,
                                              'imageSlider' => $imageSlider
                                            ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imageEdit($id)
    {
      $imageSlider = ImageSlider::where('id',$id)->first();
      return \View::make('slider.imageedit',array('slider'=>$imageSlider));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imageUpdate(Request $request)
    {
        $id = $request->input('id');
        $imageSlider = ImageSlider::find($id);
        $imageSlider->titulo = $request->input('titulo');
        $imageSlider->subtitulo = $request->input('subtitulo');
        $imageSlider->active = $request->input('active');

        if(!$imageSlider->save()){

            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            return redirect('sliderimageshowall');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $slider = Slider::find($id);
        $slider->titulo = $request->input('titulo');
        $slider->contenido = $request->input('contenido');
        $imagesSlider = $request->input('image_slider');

        if(!$slider->save()){

            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            return redirect()->back();

        }else{

            DB::table('image_sliders')
              ->where('belongs_to_slider', 1)
              ->update(['belongs_to_slider' => 0]);

            $imageSliderIndex = array_values($imagesSlider);

            DB::table('image_sliders')
              ->whereIn('id', $imageSliderIndex)
              ->update(['belongs_to_slider' => 1]);

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            return redirect('slideredit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imageDestroy($id)
    {
      $imageSlider = ImageSlider::find($id);

      if(file_exists( env('BASE_PUBLIC_DIR_PATH').'images/sliders/'.$imageSlider->nombre_imagen_guardada)){

          if(unlink(env('BASE_PUBLIC_DIR_PATH').'images/sliders/'.$imageSlider->nombre_imagen_guardada)){

              $imageSlider->delete();
              return redirect('sliderimageshowall');

          }else{

              return redirect()->back()->withError('Error Eliminando la imágen');
          }
      }else{
          return redirect()->back()->withError('Error la imagen no existe');
      }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getImages()
    {
      $header = array (
              'Content-Type' => 'application/json; charset=UTF-8',
              'charset' => 'utf-8'
          );

      $sliderGaleryAmount = env('SLIDER_IMAGE_AMOUNT');

      $imagesSlider = ImageSlider::where('active',1)
                      ->where('belongs_to_slider',1)
                      ->orderBy('id','asc')
                      ->take($sliderGaleryAmount)
                      ->get();

      $siCount = count($imagesSlider);
      $imagesSliderPrincipal = array();

      $i = 1;

      if( $siCount <  $sliderGaleryAmount){

          foreach ($imagesSlider as $r) {

            $imagesSliderPrincipal[$i]['imagename'] = $r->nombre_imagen_guardada;
            $imagesSliderPrincipal[$i]['tittle'] = $r->titulo;
            $imagesSliderPrincipal[$i]['subtittle'] = $r->subtitulo;
            $i++;
          }

          for( $i = $siCount+1; $i <= $sliderGaleryAmount; $i++){

            $imagesSliderPrincipal[$i]['imagename'] = 'default.jpg';
            $imagesSliderPrincipal[$i]['tittle'] = 'Image Tour del Café';
            $imagesSliderPrincipal[$i]['subtittle'] = 'Image Tour del Café';
          }

      }else{

        foreach ($imagesSlider as $r) {

          $imagesSliderPrincipal[$i]['imagename'] = $r->nombre_imagen_guardada;
          $imagesSliderPrincipal[$i]['tittle'] = $r->titulo;
          $imagesSliderPrincipal[$i]['subtittle'] = $r->subtitulo;
          $i++;
        }
      }

      return response()->json($imagesSliderPrincipal , 200, $header, JSON_UNESCAPED_UNICODE);
    }
}
