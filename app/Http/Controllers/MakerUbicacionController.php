<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MakerUbicacion;
use App\IconoMarker;
use App\CategoriaUbicacion;
use DB;
class MakerUbicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()

    {

        $ubicaciones = DB::table('maker_ubicacions')
            ->join('categoria_ubicacions', 'maker_ubicacions.categoria_ubicacion_id', '=', 'categoria_ubicacions.id')
            ->join('icono_markers', 'maker_ubicacions.icono_marker_id', '=', 'icono_markers.id')
            ->orderBy('id', 'asc')
            ->select('maker_ubicacions.*', 'categoria_ubicacions.nombre as categoria', 'icono_markers.nombre_imagen_guardada as nombre_icono')
            ->get();

        return \View::make('ubicacion.showall',array(
                                                        'ubicaciones' => $ubicaciones,
                                                        'isReportView'=>true
                                                    )
                            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $iconosMarker = IconoMarker::where('active',1)
                                        ->orderBy('nombre','asc')
                                        ->get();

        $categoriasUbicacion = CategoriaUbicacion::where('active',1)
                                                    ->orderBy('nombre','asc')
                                                    ->get();

        return \View::make('ubicacion.add',array(
                                                'iconosMarker' => $iconosMarker,
                                                'categoriasUbicacion' => $categoriasUbicacion
                                                )
                            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ubicacion = new MakerUbicacion();
        $ubicacion->latitud = $request->input('latitud');
        $ubicacion->longitud = $request->input('longitud');
        $ubicacion->nombre = $request->input('nombre');
        $ubicacion->descripcion = $request->input('descripcion');
        $ubicacion->direccion = $request->input('direccion');
        $ubicacion->telefono = $request->input('telefono');
        $ubicacion->transporte_publico = $request->input('transporte_publico');
        $ubicacion->categoria_ubicacion_id = $request->input('categoria_ubicacion_id');
        $ubicacion->icono_marker_id = $request->input('icono_marker_id');

        if(!$ubicacion->save()){

            return redirect()->back()->withError('Error guardando la información de la ubicación en la base de datos');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('ubicacionshowall');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id = NULL){
            return redirect('ubicacionshowall');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $iconosMarker = IconoMarker::where('active',1)
                                            ->orderBy('nombre','asc')
                                            ->get();

        $categoriasUbicacion = CategoriaUbicacion::where('active',1)
                                                    ->orderBy('nombre','asc')
                                                    ->get();

        $ubicacion = MakerUbicacion::where('id',$id)->first();


        return \View::make('ubicacion.edit',array(
                                                'ubicacion' => $ubicacion,
                                                'iconosMarker' => $iconosMarker,
                                                'categoriasUbicacion' => $categoriasUbicacion
                                                )
                            );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $ubicacion = MakerUbicacion::find($id);
        $ubicacion->latitud = $request->input('latitud');
        $ubicacion->longitud = $request->input('longitud');
        $ubicacion->nombre = $request->input('nombre');
        $ubicacion->descripcion = $request->input('descripcion');
        $ubicacion->direccion = $request->input('direccion');
        $ubicacion->telefono = $request->input('telefono');
        $ubicacion->transporte_publico = $request->input('transporte_publico');
        $ubicacion->categoria_ubicacion_id = $request->input('categoria_ubicacion_id');
        $ubicacion->icono_marker_id = $request->input('icono_marker_id');
        $ubicacion->active = $request->input('active');

        if(!$ubicacion->save()){

            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            return redirect('ubicacionshowall');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $ubicacion = MakerUbicacion::find($id);
        $ubicacion->delete();
        return redirect('ubicacionshowall');
    }

    function getByCategoria( $categoria = null ){

        $responsecode = 200;

        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
            );

        $ubicaciones = DB::table('maker_ubicacions')
            ->join('categoria_ubicacions', 'maker_ubicacions.categoria_ubicacion_id', '=', 'categoria_ubicacions.id')
            ->join('icono_markers', 'maker_ubicacions.icono_marker_id', '=', 'icono_markers.id')
            ->where('categoria_ubicacions.slug','like',"%{$categoria}%")
            ->select('maker_ubicacions.*', 'categoria_ubicacions.nombre as categoria', 'icono_markers.nombre_imagen_guardada as nombre_icono')
            ->get();


        $response = array();

        $index = 0;
        foreach ( $ubicaciones as $u){

        $response['elemento'.$index] = array
            (
                'lat' => $u->latitud,
                'lng' => $u->longitud,
                'nombre' => $u->nombre,
                'descripcion' => $u->descripcion,
                'direccion' => $u->direccion,
                'telefono' => $u->telefono,
                'iconName' => $u->nombre_icono,
                'allowBus' => ($u->transporte_publico==1)?true:false,
            );
            $index++;
        }

        return response()->json($response , 200, $header, JSON_UNESCAPED_UNICODE);
    }
}
