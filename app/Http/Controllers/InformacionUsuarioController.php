<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\InformacionUsuario;

class InformacionUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $header = array (
              'Content-Type' => 'application/json; charset=UTF-8',
              'charset' => 'utf-8'
          );

        $informacionUsuario = new InformacionUsuario();
        
        $informacionUsuario->nombre = $request->input('nombre');
        $informacionUsuario->correo_electronico = $request->input('correo_electronico');
        $informacionUsuario->fecha_nacimiento = $request->input('fecha_nacimiento');
        $informacionUsuario->telefono = $request->input('telefono');

        $status = "success";
        $message = "Información registrada correctamente";

        if(!$informacionUsuario->save()){
                    
            $status = "FAIL";
                    $message = "Error guardando la información en la base de datos";
        }

        $p = array(
                "status" => $status,
                "message" => $message,
                "timestamp" => time(),
                "data" => array(
                  "nombre" => $request->input('nombre'),
                  "correo_electronico" => $request->input('correo_electronico'),
                  "fecha_nacimiento" => $request->input('fecha_nacimiento'),
                  "telefono" => $request->input('telefono'),
                )
            );

        return response()->json($p , 200, $header, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
