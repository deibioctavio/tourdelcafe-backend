<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Informacion;
use App\Slider;
use DB;

class InformacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informaciones = Informacion::get();

        return \View::make('informacion.showall',array(
                                                        'informaciones' => $informaciones,
                                                        'isReportView'=>true
                                                    )
                            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $informacion = Informacion::where('id',$id)->first();

        return \View::make('informacion.edit',array(
                                                'informacion' => $informacion,
                                                )
                            );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $id = $request->input('id');
      $image  = $request->file('image');
      $ignoreFile = $request->input('ignore_file');

      $imageName = "";

      if( $request->file('image') == NULL || $ignoreFile == 's'){

          $informacion = Informacion::find($id);
          $informacion->titulo = $request->input('titulo');
          $informacion->contenido = $request->input('contenido');

          if(!$informacion->save()){

              $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
              return redirect()->back();

          }else{

              $request->session()->flash('flash_success_message', 'registro modificado correctamente');
              return redirect('informacioneditshowall');
          }
      }else{

        try
        {
            $extension      =   $image->getClientOriginalExtension();
            $imageRealPath  =   $image->getRealPath();
            $thumbName      =   'informacion-image-'.date('YmdHis').".".$extension;

            $img = Image::make($imageRealPath);
            $img->resize(env('IMAGE_SIZE_WIDTH'),env('IMAGE_SIZE_HEIGHT'));

            $imageSaved = $img->save(env('BASE_PUBLIC_DIR_PATH'). 'images/informacion/'. $thumbName);

            if( $imageSaved ){

                $imageName = $imageSaved->basename;

                $informacion = Informacion::find($id);
                $informacion->titulo = $request->input('titulo');
                $informacion->contenido = $request->input('contenido');
                $informacion->nombre_imagen = $thumbName;

                if(!$informacion->save()){

                    return redirect()->back()->withError('Error guardando la información de la imagen en la base de datos');

                }else{

                    $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
                    return redirect('informacioneditshowall');
                }

            }else{

                return redirect()->back()->withError('Error procesando la imagen');
            }
        }
        catch(Exception $e)
        {
            return $e;
        }

      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getInformacionContenidoSeccion($seccion){

        $responsecode = 200;

        $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
            );

        $informacion = array();

        if( $seccion == "principal"){

          $data = Slider::where('id',1)->first();

          $informacion = array(
                                "tittle" => $data->titulo,
                                "content" => $data->contenido,
                                "section" => $seccion,
                                "imagename" => "default.jpg"
                          );
        }else{

          $data = DB::table('informacions')
              ->join('categoria_informacions', 'informacions.categoria_informacion_id', '=', 'categoria_informacions.id')
              ->where('categoria_informacions.slug',$seccion)
              ->orderBy('id', 'desc')
              ->select('informacions.*')
              ->first();

          $informacion = array(
                                "tittle" => $data->titulo,
                                "content" => $data->contenido,
                                "section" => $seccion,
                                "imagename" => $data->nombre_imagen
                          );
        }

        return response()->json($informacion , 200, $header, JSON_UNESCAPED_UNICODE);

    }
}
