<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use App\Fileentry;
use Illuminate\Support\Facades\Storage;
use File;

class FileEntryController extends Controller
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$files = Fileentry::all();

		return view('fileentries.index', compact('files'));
	}


	public function add() {

		$file = Request::file('filefield');
		$extension = $file->getClientOriginalExtension();
		Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));
		$entry = new Fileentry();
		$entry->mime = $file->getClientMimeType();
		$entry->original_filename = $file->getClientOriginalName();
		$entry->filename = $file->getFilename().'.'.$extension;
		$entry->filesize = $file->getSize();
		$entry->save();
		//echo json_encode(array( "filename" =>$entry->filename));
		return redirect('fileentry');
	}

	public function addAjax() {

		$elementId = Request::input('element_id');
		$file = Request::file('filefield'.$elementId);
		$extension = $file->getClientOriginalExtension();
		Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));
		$entry = new Fileentry();
		$entry->mime = $file->getClientMimeType();
		$entry->original_filename = $file->getClientOriginalName();
		$entry->filename = $file->getFilename().'.'.$extension;
		$entry->filesize = $file->getSize();
		$entry->save();

		echo json_encode(array("filedata" =>$entry,"element_id"=>$elementId));
		//return redirect('fileentry');
	}	
}
