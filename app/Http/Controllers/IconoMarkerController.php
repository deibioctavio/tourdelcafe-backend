<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\IconoMarker;

class IconoMarkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $iconoMarkers = IconoMarker::get();

        return \View::make('iconomaker.showall',

                            array(
                                    'iconoMarkers' => $iconoMarkers,
                                    'isReportView'=>true
                                )
                        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('iconomaker.add',array());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image  = $request->file('image');

        $imageName = "";

        try
        {
            $extension      =   $image->getClientOriginalExtension();
            $imageRealPath  =   $image->getRealPath();
            $thumbName      =   'icon-marker-'.date('YmdHis').".".$extension;

            $img = Image::make($imageRealPath);
            $img->resize(env('MARKER_SIZE_WIDTH'),env('MARKER_SIZE_HEIGHT'));

            $imageSaved = $img->save(env('BASE_PUBLIC_DIR_PATH'). 'images/markers/'. $thumbName);

            if( $imageSaved ){

                $imageName = $imageSaved->basename;

                $iconoMarker = new IconoMarker();
                $iconoMarker->nombre = $request->input('nombre');
                $iconoMarker->nombre_imagen_original = $image->getClientOriginalName();
                $iconoMarker->nombre_imagen_guardada = $thumbName;

                if(!$iconoMarker->save()){

                    return redirect()->back()->withError('Error guardando la información de la imagen en la base de datos');

                }else{

                    $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
                    return redirect('iconomarkershowall');
                }

            }else{

                return redirect()->back()->withError('Error procesando la imagen');
            }
        }
        catch(Exception $e)
        {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $iconoMarker = IconoMarker::where('id',$id)->first();
        return \View::make('iconomaker.edit',array('iconoMarker'=>$iconoMarker));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $id = $request->input('id');
      $iconoMarker = IconoMarker::find($id);
      $iconoMarker->nombre = $request->input('nombre');
      $iconoMarker->active = $request->input('active');

      if(!$iconoMarker->save()){

          $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
          return redirect()->back();

      }else{

          $request->session()->flash('flash_success_message', 'registro modificado correctamente');
          return redirect('iconomarkershowall');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $iconoMarker = IconoMarker::find($id);

        if(file_exists( env('BASE_PUBLIC_DIR_PATH').'images/markers/'.$iconoMarker->nombre_imagen_guardada)){

            if(unlink(env('BASE_PUBLIC_DIR_PATH').'images/markers/'.$iconoMarker->nombre_imagen_guardada)){

                $iconoMarker->delete();
                return redirect('iconomarkershowall');

            }else{

                return redirect()->back()->withError('Error Eliminando la imágen');
            }
        }else{
            return redirect()->back()->withError('Error la imagen no existe');
        }
    }
}
