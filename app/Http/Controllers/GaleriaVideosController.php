<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GaleriaVideos;
use Intervention\Image\Facades\Image;
use DB;

class GaleriaVideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $gv = GaleriaVideos::orderBy('id','asc')->get();
      return \View::make('galeriavideos.showall',

                          array(
                                  'gv' => $gv,
                                  'isReportView'=>true
                              )
                      );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return \View::make('galeriavideos.add',array());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gv = new GaleriaVideos();
        $gv->nombre = $request->input('nombre');
        $gv->url_youtube = $request->input('url_youtube');
        $gv->posicion = $request->input('posicion');

        if(!$gv->save()){

            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
          return redirect('galeriavideosshowall');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $gv = GaleriaVideos::where('id',$id)->first();
      return \View::make('galeriavideos.edit',array('gv' => $gv));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function videosEdit($id = 1)
    {
      $gv = GaleriaVideos::where('active', 1)->orderBy('id', 'asc')->get();
      return \View::make('galeriavideos.videosedit',array(
                                                            'gv' => $gv
                                                      ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $gv = GaleriaVideos::find($id);
        $gv->nombre = $request->input('nombre');
        $gv->url_youtube = $request->input('url_youtube');
        $gv->posicion = $request->input('posicion');
        $gv->active = $request->input('active');

        if(!$gv->save()){

            return redirect()->back()->withError('Error guardando la información de la ubicación en la base de datos');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('galeriavideosshowall');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function videosUpdate(Request $request)
    {
        $videoGalery = $request->input('video_galery');

        DB::table('galeria_videos')
          ->where('belongs_to_galery', 1)
          ->update(['belongs_to_galery' => 0]);

        $videoGaleryIndex = array_values($videoGalery);

        DB::table('galeria_videos')
          ->whereIn('id', $videoGaleryIndex)
          ->update(['belongs_to_galery' => 1]);

        $request->session()->flash('flash_success_message', 'registro modificado correctamente');
        return redirect('galeriasetvideos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gv = GaleriaVideos::find($id);
        $gv->delete();
        return redirect('galeriavideosshowall');
    }

    public function getAll()
    {
      $header = array (
              'Content-Type' => 'application/json; charset=UTF-8',
              'charset' => 'utf-8'
          );

      $videosGaleria = array();
      $videoGaleryAmount = env('VIDEO_GALERY_AMOUNT');

      $gv = GaleriaVideos::where('active',1)
                      ->where('belongs_to_galery',1)
                      ->orderBy('posicion','asc')
                      ->orderBy('id','asc')
                      ->take($videoGaleryAmount)
                      ->get();
      $i = 0;
      $gvCount = count($gv);

      if( $gvCount <  $videoGaleryAmount){

          foreach ($gv as $data) {

            $videosGaleria['video'.$i]['url'] = $data->url_youtube;
            $videosGaleria['video'.$i]['nombre'] = $data->nombre;
            $i++;
          }

          for( $i = $gvCount; $i < $videoGaleryAmount; $i++){

            $videosGaleria[$i]['url'] = "8kMXL0a3x88";
            $videosGaleria[$i]['nombre'] = "Paisaje Cultural Cafetero";
          }

      }else{

        foreach ($gv as $data) {

          $videosGaleria['video'.$i]['url'] = $data->url_youtube;
          $videosGaleria['video'.$i]['nombre'] = $data->nombre;
          $i++;
        }
      }

      return response()->json($videosGaleria , 200, $header, JSON_UNESCAPED_UNICODE);
    }
}
