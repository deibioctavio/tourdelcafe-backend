<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GaleriaImagenes;
use Intervention\Image\Facades\Image;
use DB;

class GaleriaImagenesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $gi = GaleriaImagenes::orderBy('id','asc')->get();
      return \View::make('galeriaimagenes.showall',

                          array(
                                  'gi' => $gi,
                                  'isReportView'=>true
                              )
                      );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('galeriaimagenes.add',array());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $image  = $request->file('image');

      $imageName = "";

      try
      {
          $extension      =   $image->getClientOriginalExtension();
          $imageRealPath  =   $image->getRealPath();
          $thumbName      =   'galery-image-'.date('YmdHis').".".$extension;

          $img = Image::make($imageRealPath);
          $img->resize(env('IMAGE_SIZE_WIDTH'),env('IMAGE_SIZE_HEIGHT'));
          $imageSaved = $img->save(env('BASE_PUBLIC_DIR_PATH'). 'images/galeria/'. $thumbName);

          $thumbNameT      =   'galery-image-'.date('YmdHis')."t.".$extension;
          $imgT = Image::make($imageRealPath);
          $imgT->resize(env('IMAGE_THUMB_SIZE_WIDTH'),env('IMAGE_THUMB_SIZE_HEIGHT'));

          $imageThumbSaved = $imgT->save(env('BASE_PUBLIC_DIR_PATH'). 'images/galeria/'. $thumbNameT);

          if( $imageSaved && $imageThumbSaved){

              $imageName = $imageSaved->basename;

              $ig = new GaleriaImagenes();
              $ig->nombre = $request->input('nombre');
              $ig->nombre_imagen_original = $image->getClientOriginalName();
              $ig->nombre_imagen_guardada = $thumbName;
              $ig->nombre_thumb_imagen_guardada = $thumbNameT;
              $ig->posicion = $request->input('posicion');

              if(!$ig->save()){

                  return redirect()->back()->withError('Error guardando la información de la imagen en la base de datos');

              }else{

                  $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
                  return redirect('galeriaimagenesshowall');
              }

          }else{

              return redirect()->back()->withError('Error procesando la imagen');
          }
      }
      catch(Exception $e)
      {
          return $e;
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $gi = GaleriaImagenes::where('id',$id)->first();
      return \View::make('galeriaimagenes.edit',array('gi' => $gi));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imagesEdit($id = 1)
    {
      $gi = GaleriaImagenes::where('active', 1)->orderBy('id', 'asc')->get();
      return \View::make('galeriaimagenes.imagesedit',array(
                                                            'gi' => $gi
                                                      ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $gi = GaleriaImagenes::find($id);
        $gi->nombre = $request->input('nombre');
        $gi->posicion = $request->input('posicion');
        $gi->active = $request->input('active');

        if(!$gi->save()){

            return redirect()->back()->withError('Error guardando la información de la ubicación en la base de datos');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('galeriaimagenesshowall');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imagesUpdate(Request $request)
    {
        $imageGalery = $request->input('image_galery');

        DB::table('galeria_imagenes')
          ->where('belongs_to_galery', 1)
          ->update(['belongs_to_galery' => 0]);

        $imageGaleryIndex = array_values($imageGalery);

        DB::table('galeria_imagenes')
          ->whereIn('id', $imageGaleryIndex)
          ->update(['belongs_to_galery' => 1]);

        $request->session()->flash('flash_success_message', 'registro modificado correctamente');
        return redirect('galeriasetimagenes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $gi = GaleriaImagenes::find($id);

      $existImage = file_exists( env('BASE_PUBLIC_DIR_PATH').'images/galeria/'.$gi->nombre_imagen_guardada);
      $existThumb = file_exists( env('BASE_PUBLIC_DIR_PATH').'images/galeria/'.$gi->nombre_thumb_imagen_guardada);

      if( $existImage && $existThumb){

          $deleteImage = unlink(env('BASE_PUBLIC_DIR_PATH').'images/galeria/'.$gi->nombre_imagen_guardada);
          $deleteThumb = unlink(env('BASE_PUBLIC_DIR_PATH').'images/galeria/'.$gi->nombre_thumb_imagen_guardada);

          if( $deleteImage && $deleteThumb){

              $gi->delete();
              return redirect('galeriaimagenesshowall');

          }else{

              return redirect()->back()->withError('Error Eliminando la imágen');
          }
      }else{
          return redirect()->back()->withError('Error la imagen no existe');
      }
    }

    public function getAll()
    {
      $header = array (
              'Content-Type' => 'application/json; charset=UTF-8',
              'charset' => 'utf-8'
          );

      $imagesGaleria = array();
      $imageGaleryAmount = env('IMAGE_GALERY_AMOUNT');

      $gi = GaleriaImagenes::where('active',1)
                      ->where('belongs_to_galery',1)
                      ->orderBy('posicion','asc')
                      ->orderBy('id','asc')
                      ->take($imageGaleryAmount)
                      ->get();
      $i = 1;
      $giCount = count($gi);

      if( $giCount <  $imageGaleryAmount){

          foreach ($gi as $data) {

            $imagesGaleria[$i]['imagename'] = $data->nombre_imagen_guardada;
            $imagesGaleria[$i]['tittle'] = $data->nombre;
            $imagesGaleria[$i]['thumb'] = $data->nombre_thumb_imagen_guardada;
            $i++;
          }

          for( $i = $giCount+1; $i <= $imageGaleryAmount; $i++){
            $imagesGaleria[$i]['imagename'] = "default.jpg";
            $imagesGaleria[$i]['tittle'] = "Sin Imagen";
            $imagesGaleria[$i]['thumb'] = "default-200x200.jpg";
          }

      }else{

        foreach ($gi as $data) {

          $imagesGaleria[$i]['imagename'] = $data->nombre_imagen_guardada;
          $imagesGaleria[$i]['tittle'] = $data->nombre;
          $imagesGaleria[$i]['thumb'] = $data->nombre_thumb_imagen_guardada;
          $i++;
        }
      }

      return response()->json($imagesGaleria , 200, $header, JSON_UNESCAPED_UNICODE);
    }
}
