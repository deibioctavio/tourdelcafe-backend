<?php

//Default GET Calling

Route::get('/', function () {
   return redirect('home');
});
/*
//Numbers Only
Route::get('home/{id}', function($id) {
	return view("home")->with("user_id",$id);
})->where('id', '[0-9]+');
*/
//Home Route
/*
Route::get('home', function() {
	return view('home');
	//return "Welcome home";
});*/

//Home Route with Controller

Route::get('home', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);

Route::get('userroleshowall', ['as' => 'userroleshowall', 'uses' => 'UserRoleController@index','middleware' => 'role:admin']);
Route::get('userroleadd', ['as' => 'userroleadd', 'uses' => 'UserRoleController@create','middleware' => 'role:admin']);
Route::post('userroleadd', ['as' => 'userroleadd', 'uses' => 'UserRoleController@store','middleware' => 'role:admin']);
Route::get('userroleedit/{id}', ['as' => 'userroleedit', 'uses' => 'UserRoleController@edit','middleware' => 'role:admin']);
Route::post('userroleedit', ['as' => 'userroleedit', 'uses' => 'UserRoleController@update','middleware' => 'role:admin']);
Route::get('userroledelete', ['as' => 'userroledelete', 'uses' => 'UserRoleController@show','middleware' => 'role:admin']);

Route::get('profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);
Route::get('profileadd', ['as' => 'profileadd', 'uses' => 'ProfileController@create']);
Route::post('profileadd', ['as' => 'profileadd', 'uses' => 'ProfileController@store']);
Route::get('profileedit', ['as' => 'profileedit', 'uses' => 'ProfileController@edit']);
Route::post('profileedit', ['as' => 'profileedit', 'uses' => 'ProfileController@update']);
Route::get('profileshow', ['as' => 'profileshow', 'uses' => 'ProfileController@show']);
Route::post('getbyuserid', ['as' => 'getbyuserid', 'uses' => 'ProfileController@getFullProfileDataByUserId']);


Route::get('userroleshowall', ['as' => 'userroleshowall', 'uses' => 'UserRoleController@index','middleware' => 'role:admin']);
Route::get('userroleadd', ['as' => 'userroleadd', 'uses' => 'UserRoleController@create','middleware' => 'role:admin']);
Route::post('userroleadd', ['as' => 'userroleadd', 'uses' => 'UserRoleController@store','middleware' => 'role:admin']);
Route::get('userroleedit/{id}', ['as' => 'userroleedit', 'uses' => 'UserRoleController@edit','middleware' => 'role:admin']);
Route::post('userroleedit', ['as' => 'userroleedit', 'uses' => 'UserRoleController@update','middleware' => 'role:admin']);
Route::get('userroledelete', ['as' => 'userroledelete', 'uses' => 'UserRoleController@show','middleware' => 'role:admin']);

Route::get('fileentry', 'FileEntryController@index');
Route::get('fileentry/get/{filename}', ['as' => 'getentry', 'uses' => 'FileEntryController@get']);
Route::post('fileentry/add',['as' => 'addentry', 'uses' => 'FileEntryController@add']);
Route::post('fileentry/addajax',['as' => 'addentryajax', 'uses' => 'FileEntryController@addAjax']);

/*
 *
 * Ubicaciones
 */
Route::get('ubicacionadd', ['as' => 'ubicacionadd', 'uses' => 'MakerUbicacionController@create','middleware' => 'role:admin']);
Route::post('ubicacionadd', ['as' => 'ubicacionadd', 'uses' => 'MakerUbicacionController@store','middleware' => 'role:admin']);
Route::get('ubicacionedit/{id}', ['as' => 'ubicacionedit', 'uses' => 'MakerUbicacionController@edit','middleware' => 'role:admin']);
Route::post('ubicacionedit', ['as' => 'ubicacionedit', 'uses' => 'MakerUbicacionController@update','middleware' => 'role:admin']);
Route::get('ubicaciondelete/{id}', ['as' => 'ubicaciondelete', 'uses' => 'MakerUbicacionController@destroy','middleware' => 'role:admin']);
Route::get('ubicacionshow/{id}', ['as' => 'ubicacionshow', 'uses' => 'MakerUbicacionController@show','middleware' => 'role:admin']);
Route::get('ubicacionshowall', ['as' => 'ubicacionshowall', 'uses' => 'MakerUbicacionController@index','middleware' => 'role:admin']);
Route::get('ubicaciongetbycategoria/{categoria}', ['as' => 'ubicaciongetbycategoria', 'uses' => 'MakerUbicacionController@getByCategoria']);
/*
 *
 * Íconos de los Markers
 */

Route::get('iconomarkeradd', ['as' => 'iconomarkeradd', 'uses' => 'IconoMarkerController@create','middleware' => 'role:admin']);
Route::post('iconomarkeradd', ['as' => 'iconomarkeradd', 'uses' => 'IconoMarkerController@store','middleware' => 'role:admin']);
Route::get('iconomarkeredit/{id}', ['as' => 'iconomarkeredit', 'uses' => 'IconoMarkerController@edit','middleware' => 'role:admin']);
Route::post('iconomarkeredit', ['as' => 'iconomarkeredit', 'uses' => 'IconoMarkerController@update','middleware' => 'role:admin']);
Route::get('iconomarkershow/{id}', ['as' => 'iconomarkershow', 'uses' => 'IconoMarkerController@show','middleware' => 'role:admin']);
Route::get('iconomarkershowall', ['as' => 'iconomarkershowall', 'uses' => 'IconoMarkerController@index','middleware' => 'role:admin']);
Route::get('iconomarkershow/{id}', ['as' => 'iconomarkershow', 'uses' => 'IconoMarkerController@show','middleware' => 'role:admin']);

//Route::get('iconomarkerdelete/{id}', ['as' => 'iconomarkerdelete', 'uses' => 'IconoMarkerController@destroy','middleware' => 'role:admin']);

/*
 *
 * informacion
 */

Route::get('informacionedit/{id}', ['as' => 'informacionedit', 'uses' => 'InformacionController@edit','middleware' => 'role:admin']);
Route::post('informacionedit', ['as' => 'informacionedit', 'uses' => 'InformacionController@update','middleware' => 'role:admin']);
Route::get('informacioneditshowall', ['as' => 'informacioneditshowall', 'uses' => 'InformacionController@index','middleware' => 'role:admin']);
Route::get('informacioncontenidoseccion/{seccion}', ['as' => 'informacioncontenidoseccion', 'uses' => 'InformacionController@getInformacionContenidoSeccion']);

/*
 * Imágenes
 */

//Route::get('media', [ 'as' => 'image.create', 'uses' => 'ImageController@create','middleware' => 'role:admin']);
//Route::post('media', [ 'as' => 'image.store', 'uses' => 'ImageController@save','middleware' => 'role:admin']);
//Route::get('media/show', [ 'as' => 'image.resized', 'uses' => 'ImageController@show']);
//Route::get('imagesgetsliderprincipal', [ 'as' => 'imagesgetsliderprincipal', 'uses' => 'ImageController@getImagesSliderPrincipal']);
//Route::get('imagenespaquetesturisticos', [ 'as' => 'imagenespaquetesturisticos', 'uses' => 'ImageController@getImagesPaquetesTuristicos']);
//Route::get('imagenesgaleria', [ 'as' => 'imagenesgaleria', 'uses' => 'ImageController@getImagesGalery']);
//Route::get('youtubevideolinks', [ 'as' => 'youtubevideolinks', 'uses' => 'ImageController@getYoutubeVideos']);

/*
 * Slider (Created on Seeder)
 */

 //Route::get('slidershow', [ 'as' => 'slidershow', 'uses' => 'SliderController@show','middleware' => 'role:admin']);
 Route::get('slideredit/{id?}', [ 'as' => 'slideredit', 'uses' => 'SliderController@edit','middleware' => 'role:admin']);
 Route::post('slideredit', [ 'as' => 'slideredit', 'uses' => 'SliderController@update','middleware' => 'role:admin']);

Route::get('sliderimageshowall', [ 'as' => 'sliderimageshowall', 'uses' => 'SliderController@imageIndex','middleware' => 'role:admin']);
Route::get('sliderimageadd', [ 'as' => 'sliderimageadd', 'uses' => 'SliderController@imageCreate','middleware' => 'role:admin']);
Route::post('sliderimageadd', [ 'as' => 'sliderimageadd', 'uses' => 'SliderController@imageStore','middleware' => 'role:admin']);
Route::get('sliderimageedit/{id}', [ 'as' => 'sliderimageedit', 'uses' => 'SliderController@imageEdit','middleware' => 'role:admin']);
Route::post('sliderimageedit', [ 'as' => 'sliderimageedit', 'uses' => 'SliderController@imageUpdate','middleware' => 'role:admin']);
Route::get('sliderimagedelete/{id}', ['as' => 'sliderimagedelete', 'uses' => 'SliderController@imageDestroy','middleware' => 'role:admin']);
Route::get('imagesgetsliderprincipal', [ 'as' => 'imagesgetsliderprincipal', 'uses' => 'SliderController@getImages']);

/*
 * Paquetes Turísticos
 */

Route::get('paquetesturisticosshowall', [ 'as' => 'paquetesturisticosshowall', 'uses' => 'PaqueteTuristicoController@index','middleware' => 'role:admin']);
Route::get('paquetesturisticosadd', [ 'as' => 'paquetesturisticosadd', 'uses' => 'PaqueteTuristicoController@create','middleware' => 'role:admin']);
Route::post('paquetesturisticosadd', [ 'as' => 'paquetesturisticosadd', 'uses' => 'PaqueteTuristicoController@store','middleware' => 'role:admin']);
Route::get('paquetesturisticosedit/{id}', [ 'as' => 'paquetesturisticosedit', 'uses' => 'PaqueteTuristicoController@edit','middleware' => 'role:admin']);
Route::post('paquetesturisticosedit', [ 'as' => 'paquetesturisticosedit', 'uses' => 'PaqueteTuristicoController@update','middleware' => 'role:admin']);
//Route::get('paquetesturisticosenableshow', [ 'as' => 'paquetesturisticosenableshow', 'uses' => 'PaqueteTuristicoController@enableShow','middleware' => 'role:admin']);
//Route::post('paquetesturisticosenableshow', [ 'as' => 'paquetesturisticosenableshow', 'uses' => 'PaqueteTuristicoController@saveShow','middleware' => 'role:admin']);
Route::get('paquetesturisticosdelete/{id}', [ 'as' => 'paquetesturisticosdelete', 'uses' => 'PaqueteTuristicoController@destroy','middleware' => 'role:admin']);
Route::get('imagenespaquetesturisticos', [ 'as' => 'imagenespaquetesturisticos', 'uses' => 'PaqueteTuristicoController@getImages']);


/*
 * Galería de imágenes
 */

Route::get('galeriaimagenesshowall', [ 'as' => 'galeriaimagenesshowall', 'uses' => 'GaleriaImagenesController@index','middleware' => 'role:admin']);
Route::get('galeriaimagenesadd', [ 'as' => 'galeriaimagenesadd', 'uses' => 'GaleriaImagenesController@create','middleware' => 'role:admin']);
Route::post('galeriaimagenesadd', [ 'as' => 'galeriaimagenesadd', 'uses' => 'GaleriaImagenesController@store','middleware' => 'role:admin']);
Route::get('galeriaimagenesedit/{id}', [ 'as' => 'galeriaimagenesedit', 'uses' => 'GaleriaImagenesController@edit','middleware' => 'role:admin']);
Route::post('galeriaimagenesedit', [ 'as' => 'galeriaimagenesedit', 'uses' => 'GaleriaImagenesController@update','middleware' => 'role:admin']);
Route::get('galeriaimagenesdelete/{id}', [ 'as' => 'paquetesturisticosdelete', 'uses' => 'GaleriaImagenesController@destroy','middleware' => 'role:admin']);
Route::get('galeriasetimagenes', [ 'as' => 'galeriasetimagenes', 'uses' => 'GaleriaImagenesController@imagesEdit','middleware' => 'role:admin']);
Route::post('galeriasetimagenes', [ 'as' => 'galeriasetimagenes', 'uses' => 'GaleriaImagenesController@imagesUpdate','middleware' => 'role:admin']);
Route::get('imagenesgaleria', [ 'as' => 'imagenesgaleria', 'uses' => 'GaleriaImagenesController@getAll']);

/*
 * Galería de videos
 */

Route::get('galeriavideosshowall', [ 'as' => 'galeriavideosshowall', 'uses' => 'GaleriaVideosController@index','middleware' => 'role:admin']);
Route::get('galeriavideosadd', [ 'as' => 'galeriavideosadd', 'uses' => 'GaleriaVideosController@create','middleware' => 'role:admin']);
Route::post('galeriavideosadd', [ 'as' => 'galeriavideosadd', 'uses' => 'GaleriaVideosController@store','middleware' => 'role:admin']);
Route::get('galeriavideosedit/{id}', [ 'as' => 'galeriavideosedit', 'uses' => 'GaleriaVideosController@edit','middleware' => 'role:admin']);
Route::post('galeriavideosedit', [ 'as' => 'galeriavideosedit', 'uses' => 'GaleriaVideosController@update','middleware' => 'role:admin']);
Route::get('galeriavideosdelete/{id}', [ 'as' => 'galeriavideosdelete', 'uses' => 'GaleriaVideosController@destroy','middleware' => 'role:admin']);
Route::get('galeriasetvideos', [ 'as' => 'galeriasetvideos', 'uses' => 'GaleriaVideosController@videosEdit','middleware' => 'role:admin']);
Route::post('galeriasetvideos', [ 'as' => 'galeriasetvideos', 'uses' => 'GaleriaVideosController@videosUpdate','middleware' => 'role:admin']);
Route::get('youtubevideolinks', [ 'as' => 'youtubevideolinks', 'uses' => 'GaleriaVideosController@getAll']);

/*
 * informacion Usuario
 */

Route::get('verusuariosregistrados', ['as' => 'verusuariosregistrados', 'uses' => 'InformacionUsuarioController@index','middleware' => 'role:admin']);

Route::post('registrarusuario', ['as' => 'registrarusuario', 'uses' => 'InformacionUsuarioController@store']);
