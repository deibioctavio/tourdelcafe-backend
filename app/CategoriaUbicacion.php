<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaUbicacion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categoria_ubicacions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     *
     */
    protected $fillable = [
    						'nombre',
                            'slug',
    						'active'];

    public function markersubicacion() {
        
       return $this->hasMany('App\MakerUbicacion');
    }
}
