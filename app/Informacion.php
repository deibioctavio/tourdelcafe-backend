<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informacion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'informacions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     */
    protected $fillable = [
    						'titulo',
    						'contenido',
    						'nombre_imagen',
    						'categoria_informacion_id'];

    public function categoria() {

        return $this->belongsTo('App\CategoriaInformacion');
    }
}
