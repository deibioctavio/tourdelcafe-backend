<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GaleriaImagenes extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'galeria_imagenes';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   *
   *
   */
  protected $fillable = [
                          'nombre',
                          'nombre_imagen_original',
                          'nombre_imagen_guardada',
                          'nombre_thumb_imagen_guardada',
                          'posicion',
                          'active',
                          'belongs_to_galery'
                        ];
}
