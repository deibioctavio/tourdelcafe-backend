<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MakerUbicacion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'maker_ubicacions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     */
    protected $fillable = [
    						'latitud',
    						'longitud',
    						'nombre',
    						'descripcion',
    						'direccion',
    						'categoria',
    						'telefono',
    						'active'];
    						
    public function icono() {
        
       return $this->belongsTo('App\IconoMarker');
    }

    public function categoria() {

        return $this->belongsTo('App\CategoriaUbicacion');
    }
}
