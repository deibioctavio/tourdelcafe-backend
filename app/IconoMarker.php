<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IconoMarker extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'icono_markers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     *
     */
    protected $fillable = [
                						'nombre',
                            'nombre_imagen_original',
                            'nombre_imagen_guardada',
    						            'active'
                          ];

    public function markersubicacion() {

       return $this->hasMany('App\MakerUbicacion');
    }

}
