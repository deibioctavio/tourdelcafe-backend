<?php

use Illuminate\Database\Seeder;

class SliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sliders')->delete();
        DB::table('sliders')->insert(
    			[
    					'id' => 1,
    					'titulo' => 'Titulo Slider',
    				  'contenido' => 'Texto con Contenido del Slider. Cuerpo con información explicativa.'
    			]);
    }
}
