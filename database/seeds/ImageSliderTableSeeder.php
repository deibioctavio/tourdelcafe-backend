<?php

use Illuminate\Database\Seeder;

class ImageSliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('image_sliders')->delete();

        for( $i = 1; $i <= 10; $i++){
            $index = str_pad($i,2, "0",STR_PAD_LEFT);
            $images[$i]['id'] = $i;
            $images[$i]['titulo']     = "Imagen Slider ".$index;
            $images[$i]['subtitulo']  = "Subtitulo Imagen Slider ".$index;
            $images[$i]['nombre_imagen_original'] = "slider".$index.".jpg";
            $images[$i]['nombre_imagen_guardada'] = "slider".$index.".jpg";
            $images[$i]['belongs_to_slider'] = 1;
            $images[$i]['slider_id'] = 1;

            DB::table('image_sliders')->insert($images[$i]);
        }
    }
}
