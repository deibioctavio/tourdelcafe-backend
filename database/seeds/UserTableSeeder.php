<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'id' => 1,
            'name' => 'usuarioadmin',
            'email' => 'deibioctavio@gmail.com',
            'password' => bcrypt('deibioctavio'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'name' => 'administrador',
            'email' => 'juancaaa2@gmail.com',
            'password' => bcrypt('juanca7508'),
            'created_at' => getTimestamp(),
        ]);
    }
}
