<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    // php artisan db:seed
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(PermissionRouteTableSeeder::class);
        $this->call(ProfileTableSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(CategoriaUbicacionTableSeeder::class);
        $this->call(IconoMarkerTableSeeder::class);
        $this->call(CategoriaInformacionTableSeeder::class);
        $this->call(InformacionTableSeeder::class);
        $this->call(SliderTableSeeder::class);
        $this->call(ImageSliderTableSeeder::class);
        $this->call(PaquetesTuristicosTableSeeder::class);
        $this->call(GaleriaImagenesTableSeeder::class);
        $this->call(GaleriaVideosTableSeeder::class);
        
        Model::reguard();
    }
}
