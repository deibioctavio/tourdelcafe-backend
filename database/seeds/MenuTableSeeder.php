<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->delete();

        DB::table('menus')->insert([
            'id' => 1,
            'name' => 'Perfil',
            'slug' => 'profile',
            'description' => 'Menú de perfiles de usuario'
        ]);

        DB::table('menus')->insert([
            'id' => 2,
            'name' => 'Evaluación',
            'slug' => 'evaluation',
            'description' => 'Menú de Evaluaciones de desempeño laboral'
        ]);

        DB::table('menus')->insert([
            'id' => 3,
            'name' => 'Evidencia',
            'slug' => 'evidence',
            'description' => 'Menú de Evidencias de cumplimiento'
        ]);

        DB::table('menus')->insert([
            'id' => 4,
            'name' => 'Reporte',
            'slug' => 'report',
            'description' => 'Menú de Reportes Generales'
        ]);

        DB::table('menus')->insert([
            'id' => 5,
            'name' => 'Administrar',
            'slug' => 'admin',
            'description' => 'Menú de Administración'
        ]);

    }
}
