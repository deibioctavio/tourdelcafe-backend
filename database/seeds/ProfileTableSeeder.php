<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProfileTableSeeder extends Seeder {
	
	public function run() {
		
		DB::table('profiles')->delete();

        DB::table('profiles')->insert([
            'name' => 'Deibi Octavio',
            'lastname' => 'Riascos Botero',
            'document_type' => 'CC',
            'document_number' => '110001100011100',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 1
        ]);

        DB::table('profiles')->insert([
            'name' => 'Juan Carlos',
            'lastname' => 'Árias',
            'document_type' => 'CC',
            'document_number' => '220002202022200',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 2
        ]);
	}
}
?>