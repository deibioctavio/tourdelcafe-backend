<?php

use Illuminate\Database\Seeder;

class GaleriaImagenesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('galeria_imagenes')->delete();

      for( $i = 1; $i <= 18; $i++){
          $index = str_pad($i,2, "0",STR_PAD_LEFT);
          $images[$i]['nombre'] = "Imagen Galería ".$index;
          $images[$i]['nombre_imagen_original'] = "image".$index.".jpg";
          $images[$i]['nombre_imagen_guardada'] = "image".$index.".jpg";
          $images[$i]['nombre_thumb_imagen_guardada'] = "image".$index."t.jpg";
          $images[$i]['posicion'] = $i;
          $images[$i]['belongs_to_galery'] = 1;

          DB::table('galeria_imagenes')->insert($images[$i]);
      }
    }
}
