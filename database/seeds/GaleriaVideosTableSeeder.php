<?php

use Illuminate\Database\Seeder;

class GaleriaVideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('galeria_videos')->delete();
      $videos = array(
                        array(
                            'nombre' => 'Paisaje Cultural Cafetero',
                            'url_youtube' => '8kMXL0a3x88'
                        ),
                        array(
                            'nombre' => 'Colombia, Realismo Mágico',
                            'url_youtube' => 'R-rj7Oj_S_U'
                        ),
                        array(
                            'nombre' => 'Paisaje Cultural Cafetero',
                            'url_youtube' => 'lM1PhgtdJDk'
                        ),
                        array(
                            'nombre' => 'Conozca el Quindío',
                            'url_youtube' => '_vJ8xeLn3jY'
                        ),
                        array(
                            'nombre' => 'Paisaje Cultural Cafetero',
                            'url_youtube' => 'OmIy_4iSgzw'
                        ),
                );

      $i = 1;

      foreach ($videos as $v) {

        $images[$i]['nombre'] = $v['nombre'];
        $images[$i]['url_youtube'] = $v['url_youtube'];
        $images[$i]['posicion'] = $i;
        $images[$i]['belongs_to_galery'] = 1;
        DB::table('galeria_videos')->insert($images[$i]);
        $i++;

      }
    }
}
