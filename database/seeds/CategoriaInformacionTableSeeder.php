<?php

use Illuminate\Database\Seeder;

class CategoriaInformacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categoria_informacions')->delete();
       
		$categoriasInformacion =	array(               
                    					
	                    'Cultura Cafetera' => 'cultura_cafetera',
	                    'Paisaje Cultural Cafetero' => 'paisaje_cultural_cafetero',
	                    'Cafés Especiales' => 'cafes_especiales',
	                    'Entretenimiento' => 'entretenimiento',
	                    'Fiestas Regionales' => 'fiestas_regionales'
                    );

		foreach ($categoriasInformacion as $key => $value) {
			
			DB::table('categoria_informacions')->insert([
            
	            'nombre' => $key,
	            'slug' => $value
        	]);
		}   
    }
}
