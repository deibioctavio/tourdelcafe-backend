<?php

use Illuminate\Database\Seeder;

class CategoriaUbicacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categoria_ubicacions')->delete();
       
		$categoriasUbicacion =	array(               
                    					
	                    'Parque del Café' => 'parque_del_cafe',
	                    'Parques Temáticos' => 'parques_tematicos',
	                    'Centros Comerciales' => 'centros_comerciales',
	                    'Hoteles' => 'hoteles',
	                    'Restaurantes' => 'restaurantes',
	                    'Sitios de Interes -> museos' => 'sitios_interes_museos',
	                    'Sitios de Interes -> Parques Representativos' => 'sitios_interes_parques_representativos',
	                    'Sitios de Interes -> Sitios Religiosos' => 'sitios_interes_sitios_religiosos',
	                    'Cambio de Divisas' => 'cambio_divisas',
	                    'Cafés Especiales' => 'cafes_especiales',
	                    'Hospitales' => 'hospitales',
	                    'Farmacias' => 'farmacias',
	                    'Médicos -> Cirujanos' => 'medicos_cirujanos',
	                    'Médicos -> Ortopedistas' => 'medicos_ortopedistas',
	                    'Médicos -> Cardiólogos' => 'medicos_cardiologos',
	                    'Médicos -> Esteticistas' => 'medicos_esteticistas',
	                    'Medicos -> Odontólogos' => 'medicos_odontologos',
	                    'Medicos -> Acupuntura' => 'medicos_acupuntura',
	                    'Medicos -> Naturistas' => 'medicos_naturistas',
	                    'Sitios de Rumba' => 'sitios_rumba',
	                    'Termales' => 'termales',
	                    'Yipao' => 'yipao',
	                    'Deportes Extremos' => 'deportes_extremos',
	                    'Avistamiento de Aves' => 'avistamiento_aves',
	                    'Fiesta Regionales' => 'fiesta_regionales',
	                    'Chivas Rumberas' => 'chivas_rumberas',
	                    'Sitios de Interes' => 'sitios_interes',
	                    'Médicos' => 'medicos',

                    );

		foreach ($categoriasUbicacion as $key => $value) {
			
			DB::table('categoria_ubicacions')->insert([
            
	            'nombre' => $key,
	            'slug' => $value
        	]);
		}                
    }
}
