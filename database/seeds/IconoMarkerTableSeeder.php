<?php

use Illuminate\Database\Seeder;

class IconoMarkerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //icono_markers
        DB::table('icono_markers')->delete();

        $iconosMarkers =	array(               
                    					
	                    'Parques Temáticos' => 'gondola32.png',
	                    'Centros Comerciales' => 'shoppingmall32.png',
	                    'Hoteles' => 'hotel32.png',
	                    'Restaurantes' => 'restaurant32.png',
	                    'Sitios de Interes -> Museos' => 'museum32.png',
	                    'Sitios de Interes -> Parques Representativos' => 'parques-representativos32.png',
	                    'Sitios de Interes -> Sitios Religiosos' => 'iglesia32.png',
	                    'Cambio de Divisas' => 'moneyexchange32.png',
	                    'Cafés Especiales' => 'cafe-especial32.png',
	                    'Hospitales' => 'hospital32.png',
	                    'Farmacias' => 'drugstore32.png',
	                    'Médicos -> Cirujanos' => 'medic-surgeon32.png',
	                    'Médicos -> Ortopedistas' => 'medic32.png',
	                    'Médicos -> Cardiólogos' => 'medic-heart32.png',
	                    'Médicos -> Esteticistas' => 'medic32.png',
	                    'Medicos -> Odontólogos' => 'medic-theeth32.png',
	                    'Medicos -> Acupuntura' => 'medic32.png',
	                    'Medicos -> Naturistas' => 'medic32.png',
	                    'Sitios de Rumba' => 'partyclub32.png',
	                    'Termales' => 'hot-pool32.png',
	                    'Yipao' => 'jeep32.png',
	                    'Deportes Extremos' => 'paracaidismo32.png',
	                    'Avistamiento de Aves' => 'aves32.png',
	                    'Fiesta Regionales' => 'regional-party32.png',
	                    'Chivas Rumberas' => 'chiva32.png',
	                    'Sitios de Interes' => 'interestingsite32.png',
	                    'Médicos' => 'medic32.png',

                    );

        foreach ($iconosMarkers as $key => $value) {
			
			DB::table('icono_markers')->insert([
            
	            'nombre' => $key,
	            'nombre_imagen_original' => $value,
	            'nombre_imagen_guardada' => $value
        	]);
		}
    }
}
