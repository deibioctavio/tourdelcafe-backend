<?php

use Illuminate\Database\Seeder;

class PaquetesTuristicosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('paquetes_turisticos')->delete();

      for( $i = 1; $i <= 5; $i++){
          $index = str_pad($i,2, "0",STR_PAD_LEFT);
          $images[$i]['nombre'] = "Paquete Promocional ".$index;
          $images[$i]['nombre_imagen_original'] = "promo".$index.".jpg";
          $images[$i]['nombre_imagen_guardada'] = "promo".$index.".jpg";
          $images[$i]['posicion'] = $i;
          DB::table('paquetes_turisticos')->insert($images[$i]);
      }
    }
}
