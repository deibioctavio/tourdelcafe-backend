<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\CategoriaInformacion;

class InformacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lorem2 = "Texto con Contenido de la sección. Cuerpo con información explicativa.";

        DB::table('informacions')->delete();

        $categoriaInformacion = CategoriaInformacion::get();

        $i = 1;

        if($categoriaInformacion){

            foreach ($categoriaInformacion as $c) {

            	DB::table('informacions')->insert([

                'id' => $i,
                'titulo' => $c->nombre,
		            'contenido' => $lorem2,
		            'nombre_imagen' => 'default.jpg',
		            'categoria_informacion_id' => $c->id
	        	  ]);

              $i++;

            }
        }
    }
}
