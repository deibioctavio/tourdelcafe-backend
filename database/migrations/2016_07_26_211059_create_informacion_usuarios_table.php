<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformacionUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informacion_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',100);
            $table->text('correo_electronico',100);
            $table->date('fecha_nacimiento')->nullable();
            $table->string('telefono',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('informacion_usuarios');
    }
}
