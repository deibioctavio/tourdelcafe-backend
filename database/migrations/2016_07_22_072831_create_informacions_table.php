<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informacions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo',50);
            $table->text('contenido');
            $table->text('nombre_imagen');
            $table->integer('categoria_informacion_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('categoria_informacion_id')->references('id')->on('categoria_informacions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('informacions');
    }
}
