<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaquetesTuristicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paquetes_turisticos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nombre',30);
          $table->string('nombre_imagen_original',100);
          $table->string('nombre_imagen_guardada',50);
          $table->integer('posicion')->unsigned();
          $table->boolean('active')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paquetes_turisticos');
    }
}
