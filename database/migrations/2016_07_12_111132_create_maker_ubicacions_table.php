<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakerUbicacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maker_ubicacions', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->decimal('latitud',20,17);
            $table->decimal('longitud',20,17);
            $table->string('nombre',50);
            $table->string('descripcion',100);
            $table->string('direccion',100)->nullable();
            $table->string('telefono',30)->nullable();
            
            $table->boolean('transporte_publico')->default(1);
            $table->boolean('active')->default(1);

            $table->integer('categoria_ubicacion_id')->unsigned()->index();
            $table->integer('icono_marker_id')->unsigned()->index();
            $table->timestamps();
            $table->foreign('icono_marker_id')->references('id')->on('icono_markers')->onDelete('cascade');
            $table->foreign('categoria_ubicacion_id')->references('id')->on('categoria_ubicacions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('maker_ubicacions');
    }
}
