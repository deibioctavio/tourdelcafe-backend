<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_sliders', function (Blueprint $table) {
          $table->increments('id');
          $table->string('titulo',20);
          $table->string('subtitulo',30);
          $table->string('nombre_imagen_original',100);
          $table->string('nombre_imagen_guardada',50);
          $table->boolean('belongs_to_slider')->default(0);
          $table->boolean('active')->default(1);
          $table->integer('slider_id')->unsigned()->index();
          $table->timestamps();
          $table->foreign('slider_id')->references('id')->on('sliders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('image_sliders');
    }
}
