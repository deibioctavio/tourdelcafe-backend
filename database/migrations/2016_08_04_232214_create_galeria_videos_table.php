<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGaleriaVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galeria_videos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nombre',30);
          $table->string('url_youtube',30);
          $table->integer('posicion')->unsigned();
          $table->boolean('active')->default(1);
          $table->boolean('belongs_to_galery')->default(0);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('galeria_videos');
    }
}
