<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIconoMarkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icono_markers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50);
            $table->string('nombre_imagen_original',100);
            $table->string('nombre_imagen_guardada',50);
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('icono_markers');
    }
}
